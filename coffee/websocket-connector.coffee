import * as FFI from "./ffi.coffee"
import { UpstreamConnection } from "./upstream-connection.coffee"


#TODO: hide private methods
#TODO: implement timeout
#TODO: make timeout and keep-alive interval configurable

export class WebsocketConnector

	constructor: (@exchange, @uri)->
		@_status_change({ connected: false })
		@socket = undefined
		@reconnection_timer = undefined
		@reconnect_on_close = true
		@connect()

	#TOOD: make this iterable
	status: ()->
		Promise.resolve([ @status, @next_status_promise ])

	_status_change: (new_status)->
		@status = new_status
		old_next_status_promise_resolve = @next_status_promise_resolve
		@next_status_promise = new Promise (@next_status_promise_resolve)=>
		old_next_status_promise_resolve([@status, @next_status_promise])  if old_next_status_promise_resolve?

	disconnect: () ->
		@reconnect_on_close = false
		@socket.close()  if @socket?
		
	connect: ()=>
		clearTimeout(@reconnection_timer)  if @reconnection_timer?
		@reconnection_timer = undefined

		try
			@socket = new WebSocket(@uri)
		catch error
			console.error('Error connecting to remote exchange:', error) 
			@_status_change({ connected: false, error: error })
			return false

		@socket.binaryType = "arraybuffer"

		@socket.onopen = (event)=>
			console.debug('Connected to remote exchange')
			@upstream_connection = new UpstreamConnection(@exchange, "js-client", "unknown")
			@forwarding_to_network_stopped = new Promise (resolve)=>
				try 
					for await message_pointer from @upstream_connection
						@send(FFI.hakuban_message_serialize(message_pointer))
				catch error
					console.error('Error processing hakuban connection:', error)
					@socket.close()  if @socket?
				resolve()
			@_status_change({ connected: true })

		# this may fire even if onopen never did
		@socket.onclose = (event)=>
			console.debug('Disconnected from remote exchange')
			console.debug('Failed to connect to remote exchange')
			@_status_change({ connected: false, disconnect_reason: event.reason, clean_disconnect: event.wasClean })
			@socket = undefined
			if @keep_alive_interval?
				clearInterval(@keep_alive_interval)  
				@keep_alive_interval = null
			if @reconnection_timer?
				clearTimeout(@reconnection_timer)  
				@reconnection_timer = null
			if @upstream_connection?
				await @upstream_connection.drop()
				await @forwarding_to_network_stopped
				@forwarding_to_network_stopped = null
				@upstream_connection = null
			if @reconnect_on_close
				@reconnection_timer = setTimeout(@connect, 1000)

		@socket.onmessage = (event) =>
			return  if (event.data.byteLength == 0) or (event.data.length == 0) or (typeof event.data == 'string') or (event.data instanceof String)
			return  if not @upstream_connection?
			try
				await @upstream_connection.send(event.data)
			catch error
				console.error('Error processing hakuban connection:', error)
				@socket.close()  if @socket?

	ping: ()=>
		@socket.send(".")  if @socket?

	send: (bytes_to_send)=>
		@socket.send(bytes_to_send)
		clearInterval(@keep_alive_interval)  if @keep_alive_interval
		@keep_alive_interval = setInterval(@ping, 10000)
