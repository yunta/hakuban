import * as FFI from "./ffi.coffee"
import { FFIObject } from "./ffi-object.coffee"


export class ObjectDescriptor extends FFIObject

	constructor: (tags, json, pointer=null) -> 
		super()
		if pointer?
			@json = JSON.parse(FFI.hakuban_object_descriptor_json(pointer))
			@tags = (new TagDescriptor(null, null, tag_pointer) for tag_pointer in FFI.hakuban_object_descriptor_tags(pointer))
		else
			@json = json
			@tags = ((if tag instanceof TagDescriptor then tag else new TagDescriptor(tag)) for tag in tags)
			pointer = FFI.unwrap_pointer(FFI.hakuban_object_descriptor_new(JSON.stringify(@json), (tag._pointer for tag in @tags)))
		@initialize_pointer(null, pointer, FFI.hakuban_object_descriptor_drop)


export class TagDescriptor extends FFIObject

	constructor: (json) -> 
		super()
		@json = json
		pointer = FFI.unwrap_pointer(FFI.hakuban_tag_descriptor_new(JSON.stringify(@json)))
		@initialize_pointer(null, pointer, FFI.hakuban_tag_descriptor_drop)

