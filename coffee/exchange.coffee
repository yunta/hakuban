import * as FFI from "./ffi.coffee"
import { FFIObject } from "./ffi-object.coffee"
import { ObjectObserveContract, ObjectExposeContract, TagObserveContract, TagExposeContract } from "./contract.coffee"


export class Exchange extends FFIObject

	constructor: (name="wasm") ->
		super()
		@name = name
		@_notification_sequence = BigInt(0)
		@_notification_resolve = {}
		@initialize_pointer(null, FFI.unwrap_pointer(FFI.hakuban_exchange_new(@name)),FFI.hakuban_exchange_drop)

	object_observe_contract: (descriptor_or_tags, json_or_nothing) ->
		new ObjectObserveContractBuilder(@, [descriptor_or_tags, json_or_nothing])

	object_expose_contract: (descriptor_or_tags, json_or_nothing) ->
		new ObjectExposeContractBuilder(@, [descriptor_or_tags, json_or_nothing])

	tag_observe_contract: (descriptor) ->
		new TagObserveContractBuilder(@, descriptor)

	tag_expose_contract: (descriptor) ->
		new TagExposeContractBuilder(@, descriptor)

	notified: ()->
		id = ++@_notification_sequence
		promise = new Promise((resolve)=>@_notification_resolve[id]=resolve)
		promise.id = id
		promise

	notify: ()->
		for id in @with_pointer (pointer)->FFI.hakuban_exchange_notified(pointer)
			# if you want to put console.log here, you forgot to await
			if @_notification_resolve[id]?
				@_notification_resolve[id](true)
				delete @_notification_resolve[id]
	
	return_notified: (notification_promise)->
		delete @_notification_resolve[notification_promise.id]


class ObjectObserveContractBuilder
	constructor: (@exchange, @descriptor) -> null
	build: (block) -> new ObjectObserveContract(@exchange, @descriptor).do_and_drop_or_return(block)

class ObjectExposeContractBuilder
	constructor: (@exchange, @descriptor) -> @capacity = 1
	with_capacity: (capacity) -> @capacity = capacity
	build: (block) -> new ObjectExposeContract(@exchange, @descriptor, @capacity).do_and_drop_or_return(block)


class TagObserveContractBuilder
	constructor: (@exchange, @descriptor) -> null
	build: (block) -> new TagObserveContract(@exchange, @descriptor).do_and_drop_or_return(block)

class TagExposeContractBuilder
	constructor: (@exchange, @descriptor) -> @capacity = 1
	with_capacity: (capacity) -> @capacity = capacity
	build: (block) -> new TagExposeContract(@exchange, @descriptor, @capacity).do_and_drop_or_return(block)
