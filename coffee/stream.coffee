import * as FFI from "./ffi.coffee"
import { PointerAlreadyDropped } from "./ffi-object.coffee"

export class Stream

	@mixin: (new_object, _ffi_next, _item_constructor, debug_description)->

		# Rust StreamExt-like methods

		new_object.next = (block)->
			try
				future_pointer = @with_pointer (pointer)=>_ffi_next(pointer)
				pointer = await new FFI.Future(@exchange, @, FFI.hakuban_future_poll, FFI.hakuban_future_drop, future_pointer, debug_description+".next").await_and_drop()
				if pointer?
					if _item_constructor?
						await new _item_constructor(@exchange, pointer).do_and_drop_or_return(block)
					else 
						pointer
				else
					null
			catch error
				return null  if error instanceof PointerAlreadyDropped
				throw error


		new_object.for_each_concurrent = (block)->

			#TODO: simplify when we get rid of coffee
			task = (tasks, id, item)->
				new Promise (_resolve)=>
					try
						await block(item)
					finally
						await item.drop()
						delete tasks[id]

			tasks = {}
			sequence = 0
			for await item from @
				id = ++sequence
				tasks[id] = task(tasks, id, item)
				
			await Promise.all(Object.values(tasks))


		new_object.for_each = (block)->
			for await item from @
				try
					await block(item)
				finally
					await item.drop()


		# JS AsyncIterator methods

		class StreamAsyncIterator
	
			constructor: (stream)->

				@next = ()->
					item = await stream.next()
					if item?
						{ done: false, value: item }
					else
						{ done: true }

				@return = (value)->
					stream.drop()
					{ done: true, value: value }

		new_object[Symbol.asyncIterator]=()-> new StreamAsyncIterator(@)




