import * as FFI from "./ffi.coffee"
import { FFIObject } from "./ffi-object.coffee"


ObjectState_last_generated_version = [0,0,0]

#TODO: make this immutable for the users somehow
export class ObjectState extends FFIObject

	constructor: (data=null, version=null, format=null, synchronized_us_ago=0, pointer=null)->
		super()
		if pointer?
			@initialize_pointer(null, pointer, FFI.hakuban_object_state_drop)
			@version = (x for x in FFI.hakuban_object_state_version(pointer))
			@synchronized_us_ago = FFI.hakuban_object_state_synchronized_ago(pointer)
			@format = FFI.hakuban_object_state_format(pointer)
			@data = new TextDecoder().decode(FFI.hakuban_object_state_data(pointer))
		else
			if !version?
				timestamp = new Date().getTime()
				version = [0, Math.floor(timestamp/1000), (timestamp % 1000) * 1000]
				version[2]++  while version[1] == ObjectState_last_generated_version[1] and version[2] <= ObjectState_last_generated_version[2]
				ObjectState_last_generated_version = version
			@version = (BigInt(version_element) for version_element in version)
			if !format?
				format = []
			@format = [format].flat(Infinity)
			@synchronized_us_ago = BigInt(synchronized_us_ago)
			@data = data

	@from_pointer: (_exchange, pointer)->
		new ObjectState(null, null, null, null, pointer)

	with_pointer: (block)->
		throw new PointerAlreadyDropped()  if @dropped()
		if @_pointer?
			block(@_pointer)
		else
			pointer = FFI.unwrap_pointer(FFI.hakuban_object_state_new(@version, @format, new TextEncoder().encode(@data), @synchronized_us_ago))
			@initialize_pointer(null, pointer, FFI.hakuban_object_state_drop)
			block(@_pointer)

	with_data: (data)->
		new ObjectState(data, @version, @format, @synchronized_us_ago)

	with_version: (version)->
		new ObjectState(@data, version, @format, @synchronized_us_ago)

	with_format: (format)->
		new ObjectState(@data, @version, format, @synchronized_us_ago)

	with_synchronized_us_ago: (synchronized_us_ago)->
		new ObjectState(@data, @version, @format, synchronized_us_ago)

	json_deserialize: ()->
		throw "Invalid data format"  if @format[@format.length - 1] != "JSON"
		new ObjectState(JSON.parse(@data), @version, @format.slice(0, -1), @synchronized_us_ago)

	json_serialize: ()->
		new ObjectState(JSON.stringify(@data), @version, @format.concat("JSON"), @synchronized_us_ago)
