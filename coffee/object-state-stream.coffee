import * as FFI from "./ffi.coffee"
import { FFIObject } from "./ffi-object.coffee"
import { ObjectDescriptor } from "./descriptor.coffee"
import { ObjectState } from "./object-state.coffee"
import { Stream } from "./stream.coffee"


export class ObjectStateStream extends FFIObject

	constructor: (exchange, pointer)->
		super()
		Stream.mixin(@, FFI.hakuban_object_state_stream_next, ((_exchange, pointer)->new ObjectState(null, null, null, null, pointer)), "ObjectStateStream")
		@exchange = exchange
		@initialize_pointer(@exchange, pointer, FFI.hakuban_object_state_stream_drop)

	descriptor: ()->
		@with_pointer (pointer)->new ObjectDescriptor(null, null, FFI.hakuban_object_state_stream_descriptor(pointer))

