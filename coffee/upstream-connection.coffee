import * as FFI from "./ffi.coffee"
import { FFIObject, PointerAlreadyDropped } from "./ffi-object.coffee"
import { Stream } from "./stream.coffee"


export class UpstreamConnection extends FFIObject

	constructor: (exchange, local_name, local_address)->
		super()
		Stream.mixin(@, FFI.hakuban_upstream_connection_next_message_to_network, undefined, "UpstreamConnection")
		@exchange = exchange
		@exchange.with_pointer (exchange_pointer)=>
			@initialize_pointer(@exchange, FFI.hakuban_upstream_connection_new(exchange_pointer, local_name, local_address), FFI.hakuban_upstream_connection_drop)

	send: (data)->
		try
			future_pointer = @with_pointer (pointer)=>FFI.hakuban_upstream_connection_send_message_from_network(pointer, new Uint8Array(data))
			await new FFI.Future(@exchange, @, FFI.hakuban_future_poll, FFI.hakuban_future_drop, future_pointer, "UpstreamConnection.send").await_and_drop()
		catch error
			return null  if error instanceof PointerAlreadyDropped
			throw error

