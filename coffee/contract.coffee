import * as FFI from "./ffi.coffee"
import { FFIObject } from "./ffi-object.coffee"
import { ObjectDescriptor, TagDescriptor } from "./descriptor.coffee"
import { ObjectStateStream } from "./object-state-stream.coffee"
import { ObjectStateSink } from "./object-state-sink.coffee"
import { Stream } from "./stream.coffee"

	
export class ObjectObserveContract extends FFIObject

	constructor: (exchange, descriptor) ->
		super()
		Stream.mixin(@, FFI.hakuban_object_observe_contract_next, ObjectStateStream)
		@exchange = exchange
		@descriptor = if descriptor[0] instanceof ObjectDescriptor
			descriptor[0]
		else
			new ObjectDescriptor(descriptor...)  
		@exchange.with_pointer (exchange_pointer)=>
			@descriptor.with_pointer (descriptor_pointer)=>	
				@initialize_pointer(@exchange, FFI.hakuban_object_observe_contract_new(exchange_pointer, descriptor_pointer), FFI.hakuban_object_observe_contract_drop)
		@exchange.notify()
		


export class ObjectExposeContract extends FFIObject

	constructor: (exchange, descriptor, capacity)->
		super()
		Stream.mixin(@, FFI.hakuban_object_expose_contract_next, ObjectStateSink)
		@exchange = exchange
		@descriptor = if descriptor[0] instanceof ObjectDescriptor
			descriptor[0]
		else
			new ObjectDescriptor(descriptor...)
		@exchange.with_pointer (exchange_pointer)=>
			@descriptor.with_pointer (descriptor_pointer)=>
				@initialize_pointer(@exchange, FFI.hakuban_object_expose_contract_new(exchange_pointer, descriptor_pointer, capacity), FFI.hakuban_object_expose_contract_drop)
		@exchange.notify()



export class TagObserveContract extends FFIObject

	constructor: (exchange, descriptor) ->
		super()
		Stream.mixin(@, FFI.hakuban_tag_observe_contract_next, ObjectStateStream)
		@exchange = exchange
		@descriptor = descriptor
		@descriptor = new TagDescriptor(@descriptor)  if not (@descriptor instanceof TagDescriptor)
		@exchange.with_pointer (exchange_pointer)=>
			@descriptor.with_pointer (descriptor_pointer)=>
				@initialize_pointer(@exchange, FFI.hakuban_tag_observe_contract_new(exchange_pointer, descriptor_pointer), FFI.hakuban_tag_observe_contract_drop)
		@exchange.notify()



export class TagExposeContract extends FFIObject

	constructor: (exchange, descriptor, capacity) ->
		super()
		Stream.mixin(@, FFI.hakuban_tag_expose_contract_next, ObjectStateSink)
		@exchange = exchange
		@descriptor = descriptor
		@descriptor = new TagDescriptor(@descriptor)  if not (@descriptor instanceof TagDescriptor)
		@exchange.with_pointer (exchange_pointer)=>
			@descriptor.with_pointer (descriptor_pointer)=>
				@initialize_pointer(@exchange, FFI.hakuban_tag_expose_contract_new(exchange_pointer, descriptor_pointer, capacity), FFI.hakuban_tag_expose_contract_drop)
		@exchange.notify()
