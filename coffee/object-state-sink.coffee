import * as FFI from "./ffi.coffee"
import { FFIObject, PointerAlreadyDropped } from "./ffi-object.coffee"
import { ObjectDescriptor } from "./descriptor.coffee"
import { Stream } from "./stream.coffee"


export class ObjectStateSinkParams extends FFIObject

	constructor: (_exchange,pointer)->
		super()
		@initialize_pointer(null, pointer, FFI.hakuban_object_state_sink_params_drop)


export class ObjectStateSink extends FFIObject

	constructor: (exchange, pointer)->
		super()
		Stream.mixin(@, FFI.hakuban_object_state_sink_next, ObjectStateSinkParams)
		@exchange = exchange
		@initialize_pointer(@exchange, pointer, FFI.hakuban_object_state_sink_drop)

	descriptor: ()->
		@with_pointer (pointer)->new ObjectDescriptor(null, null, FFI.hakuban_object_state_sink_descriptor(pointer))

	send: (object_state)->
		try
			future_pointer = @with_pointer (sink_pointer)=>
				object_state.with_pointer (object_state_pointer)=>
					FFI.hakuban_object_state_sink_push(sink_pointer, object_state_pointer)
			await new FFI.Future(@exchange, @, FFI.hakuban_future_poll, FFI.hakuban_future_drop, future_pointer).await_and_drop()
			true
		catch error
			return null  if error instanceof PointerAlreadyDropped
			throw error

