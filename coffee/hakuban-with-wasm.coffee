import * as hakuban_js from "../pkg/hakuban.js"
import hakuban_wasm_gzip_base64 from "../hakuban.wasm.gz"
import { logger_initialize } from "./logger.coffee"

export initialize = (log_level)->
	wasm_array = new Blob(await Array.fromAsync((await fetch('data:;base64,'+hakuban_wasm_gzip_base64)).body.pipeThrough(new DecompressionStream("gzip")))).arrayBuffer()
	result = await hakuban_js.default(wasm_array)
	logger_initialize(log_level)
	result

export { ObjectObserveContract, ObjectExposeContract, TagObserveContract, TagExposeContract } from "./contract.coffee"
export { ObjectDescriptor, TagDescriptor } from "./descriptor.coffee"
export { Exchange } from "./exchange.coffee"
export { ObjectState } from "./object-state.coffee"
export { WebsocketConnector } from "./websocket-connector.coffee"


