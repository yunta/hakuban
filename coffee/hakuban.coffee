import * as hakuban_js from "../pkg/hakuban.js"
import { logger_initialize } from "./logger.coffee"

export initialize = (wasm, log_level)->
	result = await hakuban_js.default(wasm)
	logger_initialize(log_level)
	result


export { ObjectObserveContract, ObjectExposeContract, TagObserveContract, TagExposeContract } from "./contract.coffee"
export { ObjectDescriptor, TagDescriptor } from "./descriptor.coffee"
export { Exchange } from "./exchange.coffee"
export { ObjectState } from "./object-state.coffee"
export { WebsocketConnector } from "./websocket-connector.coffee"
