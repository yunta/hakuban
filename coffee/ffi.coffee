import { log_level } from "./logger.coffee"

export * from "../pkg/hakuban.js"

export unwrap_pointer = (result)->
	if result.status == 1
		result.pointer
	else
		throw "hakuban wasm call failed: "+result.status

		

export class Future

	constructor: (@exchange, @future_owner, @poll_function, @drop_function, @_pointer, @debug_description)->

	await_and_drop: (aborted=null)->
		try
			about_to_drop_notification_received = new Promise (about_to_drop_notification)=>
				@lock_id = @future_owner.drop_lock(about_to_drop_notification)
			while true
				notified = @exchange.notified()
				console.debug("polling " + @debug_description, notified.id)  if @debug_description and log_level() == "trace"
				result = @exchange.with_pointer (exchange_pointer)=> @poll_function(exchange_pointer, @_pointer, notified.id)
				console.debug("polled " + @debug_description, notified.id, result.status)  if @debug_description and log_level() == "trace"
				@exchange.notify()
				switch result.status
					when 0 # ok
						return true
					when 1 # pointer
						return result.pointer
					when 2 # pending
						to_listen = [notified, about_to_drop_notification_received]
						to_listen.push(aborted)  if aborted?
						switch await Promise.race(to_listen)
							when "ABORT" then return false
							when "DROP" then return null
					when 3 # end of stream
						return null
					else
						console.error(result.error_message)
						throw result.status  #TODO: convert that to specific error
		finally
			@drop_function(@_pointer)
			@future_owner.drop_release(@lock_id)
			@exchange.return_notified(notified)  if notified?

