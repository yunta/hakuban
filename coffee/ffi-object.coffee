
finalization_registry = new FinalizationRegistry (function_and_arg)-> function_and_arg[0](function_and_arg[1])


do_and_drop = (object_or_objects, block)->
		try
			block_returned_promise = false
			ret = block(object_or_objects)
			if ret? and typeof ret.then == 'function'
				block_returned_promise = true
				ret.finally ()=>
					for object in [object_or_objects].flat()
						object.drop()
			else
				ret
		finally
			if not block_returned_promise
				for object in [object_or_objects].flat()
					object.drop()


do_and_drop_or_return = (object_or_objects, block)->
		if block?
			do_and_drop(object_or_objects, block)
		else
			object_or_objects


export class PointerAlreadyDropped
	constructor: ()->@message="PointerAlreadyDropped"


export class FFIObject


	initialize_pointer: (@_exchange, @_pointer, @_drop_function)->
		@_was_dropped = null
		@_drop_locks_sequence = 0
		@_drop_locks = {}
		finalization_registry.register(@, [@_drop_function, @_pointer], @)
	
	dropped: ()->
		@_was_dropped?

	drop: ()-> 
		if not @_was_dropped?
			@_was_dropped = new Promise (resolve)=>
				if @_pointer?
					if Object.keys(@_drop_locks).length > 0
						all_locks_dropped = new Promise (@_all_locks_dropped_set)=>
						for about_to_drop_notification in Object.values(@_drop_locks)
							about_to_drop_notification("DROP")
						await all_locks_dropped
					throw "Hakuban internal error: drop was attempted with existing drop locks"  if Object.keys(@_drop_locks).length > 0
					finalization_registry.unregister(@)
					@_drop_function(@_pointer)
					@_exchange.notify()  if @_exchange?
				@_pointer = false
				resolve()
		@_was_dropped

	with_pointer: (block)->
		throw new PointerAlreadyDropped()  if @dropped()
		block(@_pointer)

	do_and_drop_or_return: (block)->
		do_and_drop_or_return(@, block)

	drop_lock: (about_to_drop_notification)->
		raise FFIObject::PointerAlreadyDropped  if @_was_dropped?
		lock_id = @_drop_locks_sequence++
		@_drop_locks[lock_id] = about_to_drop_notification
		lock_id
	
	drop_release: (lock_id)->
		delete @_drop_locks[lock_id]
		if @_was_dropped? and Object.keys(@_drop_locks).length == 0
			@_all_locks_dropped_set()
