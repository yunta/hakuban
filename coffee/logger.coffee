import * as FFI from "./ffi.coffee"

logger_initialized = false
logger_log_level = undefined

export logger_initialize = (log_level, skip_if_already_initialized=false) ->
	if !logger_initialized || !skip_if_already_initialized 
		result = FFI.hakuban_logger_initialize(log_level)
		if result.status == 0
			logger_initialized = true
			logger_log_level = log_level
		else
			console.error "Failed to initialize logger, error " + result.error_message

export log_level = () -> logger_log_level

