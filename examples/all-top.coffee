import * as hakuban from "../hakuban-with-wasm.js"
import m from "mithril"


window.onload = ()->
	await hakuban.initialize("info")
	@exchange = new hakuban.Exchange("all-top.html")
	conn = new hakuban.WebsocketConnector(@exchange, "ws://127.0.0.1:3001")

	observe_contract = @exchange.tag_observe_contract("utilizations").build()
	hosts = {}

	observe_contract.for_each_concurrent (host_utilization_stream)->
		host_id = host_utilization_stream.descriptor().json.host + ":" + host_utilization_stream.descriptor().json.process
		await host_utilization_stream.for_each (host_utilization_state)->
			hosts[host_id] = host_utilization_state.json_deserialize().data
			m.render(document.body, m(Page, {hosts: hosts}))  
		delete hosts[host_id]
		m.render(document.body, m(Page, {hosts: hosts}))  


class Page
	view: (vnode)->
		m "div", style: { display: "grid", "grid-template-columns": "max-content auto", margin: "2em" },
			for host_id in Object.keys(vnode.attrs.hosts).sort()
				[
					m "div", style: { "padding-right": "2em" }, host_id
					m "div", style: { display: "flex" },
						for load in vnode.attrs.hosts[host_id].percentage
							m "div", style: { width: "1em" },
								m "div", style: { width: "100%", background: "#008", height: (100-load)+"%" }
								m "div", style: { width: "100%", background: "#FF0", height: (load)+"%" }
				]

