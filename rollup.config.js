import { base64 } from 'rollup-plugin-base64';
import coffee from 'rollup-plugin-coffee-script';
import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import terser from '@rollup/plugin-terser';

const lib_with_wasm = () => ({
  input: "coffee/hakuban-with-wasm.coffee",
  plugins: [
    base64({ include: "hakuban.wasm.gz" }),
    coffee(),
  ],
  output: [
    { format: "es", file: `hakuban-with-wasm.js`, sourcemap: true },
    { format: "es", file: `hakuban-with-wasm.min.js`, plugins: [terser()], sourcemap: true },
  ],
});

const lib_without_wasm = (fmt) => ({
  input: "coffee/hakuban.coffee",
  plugins: [
    coffee(),
  ],
  output: [
    { format: "es", file: `hakuban.js`, sourcemap: true },
    { format: "es", file: `hakuban.min.js`, plugins: [terser()], sourcemap: true },
  ],
});


const embedded = (dir, file)=> ({
  input: dir+file+".coffee",
  output: { dir: `./`, format: 'esm', name: "hakuban-"+file },
  plugins: [
    coffee(),
    nodeResolve({extensions: ['.js']}),
    commonjs({extensions: ['.js']}),
    {
      generateBundle(opts, bundle) {
        this.emitFile({
          type: "asset",
            fileName: file+".html",
          source: '<html><head><meta charset="utf-8" /></head><body><script type="module">'+bundle[file+".js"].code+"</script></body></html>"
        });
      }
    }
  ],
});


export default [lib_with_wasm(), lib_without_wasm(), embedded("examples/","all-top"), embedded("tests/","basic")];
