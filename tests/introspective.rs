extern crate hakuban;

mod common;
use std::{error::Error, time::Duration};

use common::{network::Topology, router::Router, shuffler::Shuffle};
use futures::{SinkExt, StreamExt};
use hakuban::{
	monitor::ExchangeSnapshot,
	tokio_runtime::{
		monitor::{WebsocketConnectorSnapshot, WebsocketListenerSnapshot},
		WebsocketConnector,
	},
	Exchange, JsonDeserializeState, JsonSerializeState, ObjectDescriptor, ObjectState, TagDescriptor,
};
use serde_json::json;

#[tokio::test]
async fn router_publishes_monitoring_objects() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "", true, 0).await;
			Ok(router)
		})
		.task("regu", |mut _task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), router.url.clone())?;
			let mut exchange_monitor_contract = hakuban_a.object_observe_contract((vec![json!("monitor")], json!("exchange"))).build();
			let mut exchange_snapshots = exchange_monitor_contract.next().await.unwrap();
			let exchange_snapshot = exchange_snapshots.next().await.unwrap().json_deserialize::<ExchangeSnapshot>();
			assert_eq!(exchange_snapshot.data.objects.len(), 2);
			assert_eq!(exchange_snapshot.data.tags.len(), 1);
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(2000))
		.await
}

#[tokio::test]
async fn names_get_propagated() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router-name-xxx", ["--monitor-tag=monitor".to_string()], "", true, 0).await;
			Ok(router)
		})
		.task("regu", |mut _task, router| async move {
			let hakuban_a = Exchange::new();
			let upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().as_str().to_string() + "#name=regu").as_str())?;
			while upstream_a.snapshot().upstream_connection.map(|connection| connection.local_name) != Some("regu".to_string()) {
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			while upstream_a.snapshot().upstream_connection.map(|connection| connection.remote_name) != Some(Some("router-name-xxx".to_string())) {
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			let mut listener_monitor_contract = hakuban_a.object_observe_contract((vec![json!("monitor")], json!("websocket-listener"))).build();
			let mut listener_snapshots = listener_monitor_contract.next().await.unwrap();
			while listener_snapshots.next().await.unwrap().json_deserialize::<WebsocketListenerSnapshot>().data.downstream_connections[0].remote_name
				!= Some("regu".to_string())
			{
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(2000))
		.await
}

#[tokio::test]
async fn object_gets_dropped_when_no_more_needed_with_object_to_object_pairing() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "", true, 0).await;
			Ok(router)
		})
		.task("monitor", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().as_str().to_string() + "#name=monitor").as_str())?;
			let mut exchange_monitor_contract = hakuban_a.object_observe_contract((vec![json!("monitor")], json!("exchange"))).build();
			let mut exchange_snapshots = exchange_monitor_contract.next().await.unwrap();
			while exchange_snapshots.next().await.unwrap().json_deserialize::<ExchangeSnapshot>().data.objects.len() != 3 {
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			while exchange_snapshots.next().await.unwrap().json_deserialize::<ExchangeSnapshot>().data.objects.len() != 2 {
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			Ok(())
		})
		.task("regu", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().as_str().to_string() + "#name=regu").as_str())?;
			let object_observe_contract = hakuban_a.object_observe_contract((vec![json!("tag")], json!("object"))).build();
			task.barrier().await;
			drop(object_observe_contract);
			task.barrier().await;
			Ok(())
		})
		.task("riko", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().to_string() + "#name=riko").as_str())?;
			let object_expose_contract = hakuban_a.object_expose_contract((vec![json!("tag")], json!("object"))).build();
			task.barrier().await;
			drop(object_expose_contract);
			task.barrier().await;
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(4000))
		.await
}

#[tokio::test]
async fn object_gets_dropped_when_no_more_needed_with_object_to_tag_pairing() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "", true, 0).await;
			Ok(router)
		})
		.task("monitor", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), router.url.clone())?;
			let mut exchange_monitor_contract = hakuban_a.object_observe_contract((vec![json!("monitor")], json!("exchange"))).build();
			let mut exchange_snapshots = exchange_monitor_contract.next().await.unwrap();
			while exchange_snapshots.next().await.unwrap().json_deserialize::<ExchangeSnapshot>().data.objects.len() != 3 {
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			while exchange_snapshots.next().await.unwrap().json_deserialize::<ExchangeSnapshot>().data.objects.len() != 2 {
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			Ok(())
		})
		.task("regu", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), router.url.clone())?;
			let object_observe_contract = hakuban_a.object_observe_contract((vec![json!("tag")], json!("object"))).build();
			task.barrier().await;
			drop(object_observe_contract);
			task.barrier().await;
			Ok(())
		})
		.task("riko", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), router.url.clone())?;
			let tag_expose_contract = hakuban_a.tag_expose_contract(json!("tag")).build();
			task.barrier().await;
			drop(tag_expose_contract);
			task.barrier().await;
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(4000))
		.await
}

#[tokio::test]
async fn object_gets_dropped_when_no_more_needed_with_tag_to_object_pairing() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "", true, 0).await;
			Ok(router)
		})
		.task("monitor", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), router.url.clone())?;
			let mut exchange_monitor_contract = hakuban_a.object_observe_contract((vec![json!("monitor")], json!("exchange"))).build();
			let mut exchange_snapshots = exchange_monitor_contract.next().await.unwrap();
			while exchange_snapshots.next().await.unwrap().json_deserialize::<ExchangeSnapshot>().data.objects.len() != 3 {
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			while exchange_snapshots.next().await.unwrap().json_deserialize::<ExchangeSnapshot>().data.objects.len() != 2 {
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			Ok(())
		})
		.task("regu", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), router.url.clone())?;
			let tag_observe_contract = hakuban_a.tag_observe_contract(json!("tag")).build();
			task.barrier().await;
			drop(tag_observe_contract);
			task.barrier().await;
			Ok(())
		})
		.task("riko", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), router.url.clone())?;
			let object_expose_contract = hakuban_a.object_expose_contract((vec![json!("tag")], json!("object"))).build();
			task.barrier().await;
			drop(object_expose_contract);
			task.barrier().await;
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(4000))
		.await
}

#[tokio::test]
async fn object_expose_capacity_gets_propagated_upstream() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "", true, 0).await;
			Ok(router)
		})
		.task("monitor", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().as_str().to_string() + "#name=monitor").as_str())?;
			let mut exchange_monitor_contract = hakuban_a.object_observe_contract((vec![json!("monitor")], json!("exchange"))).build();
			let mut exchange_snapshots = exchange_monitor_contract.next().await.unwrap();
			while exchange_snapshots.next().await.unwrap().json_deserialize::<ExchangeSnapshot>().data.objects.len() != 3 {
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			let descriptor = ObjectDescriptor::new(vec![json!("tag")], json!("object"));
			let object_snapshot = exchange_snapshots
				.next()
				.await
				.unwrap()
				.json_deserialize::<ExchangeSnapshot>()
				.data
				.objects
				.into_iter()
				.find(|object| object.descriptor == descriptor)
				.unwrap();
			assert_eq!(object_snapshot.cumulative_object_exposer_capacity, 1);
			task.barrier().await;
			while exchange_snapshots
				.next()
				.await
				.unwrap()
				.json_deserialize::<ExchangeSnapshot>()
				.data
				.objects
				.into_iter()
				.find(|object| object.descriptor == descriptor)
				.unwrap()
				.cumulative_object_exposer_capacity
				!= 2
			{
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			while exchange_snapshots
				.next()
				.await
				.unwrap()
				.json_deserialize::<ExchangeSnapshot>()
				.data
				.objects
				.into_iter()
				.find(|object| object.descriptor == descriptor)
				.unwrap()
				.cumulative_object_exposer_capacity
				!= 3
			{
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			while exchange_snapshots
				.next()
				.await
				.unwrap()
				.json_deserialize::<ExchangeSnapshot>()
				.data
				.objects
				.into_iter()
				.find(|object| object.descriptor == descriptor)
				.unwrap()
				.cumulative_object_exposer_capacity
				!= 2
			{
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			while exchange_snapshots
				.next()
				.await
				.unwrap()
				.json_deserialize::<ExchangeSnapshot>()
				.data
				.objects
				.into_iter()
				.find(|object| object.descriptor == descriptor)
				.unwrap()
				.cumulative_object_exposer_capacity
				!= 1
			{
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			Ok(())
		})
		.task("regu", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().as_str().to_string() + "#name=regu").as_str())?;
			let _object_expose_contract = hakuban_a.object_expose_contract((vec![json!("tag")], json!("object"))).build();
			task.barrier().await;
			task.barrier().await;
			task.barrier().await;
			task.barrier().await;
			task.barrier().await;
			Ok(())
		})
		.task("riko", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().to_string() + "#name=riko").as_str())?;
			task.barrier().await;
			let object_expose_contract_1 = hakuban_a.object_expose_contract((vec![json!("tag")], json!("object"))).build();
			task.barrier().await;
			let _object_expose_contract_2 = hakuban_a.object_expose_contract((vec![json!("tag")], json!("object"))).build();
			task.barrier().await;
			drop(object_expose_contract_1);
			task.barrier().await;
			drop(upstream_a);
			task.barrier().await;
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(10000))
		.await
}

#[tokio::test]
async fn tag_expose_capacity_gets_propagated_upstream() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "", true, 0).await;
			Ok(router)
		})
		.task("monitor", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().as_str().to_string() + "#name=monitor").as_str())?;
			let mut exchange_monitor_contract = hakuban_a.object_observe_contract((vec![json!("monitor")], json!("exchange"))).build();
			let mut exchange_snapshots = exchange_monitor_contract.next().await.unwrap();
			while exchange_snapshots.next().await.unwrap().json_deserialize::<ExchangeSnapshot>().data.objects.len() != 3 {
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			let tag_descriptor = TagDescriptor::new(json!("tag"));
			let object_descriptor = ObjectDescriptor::new(vec![tag_descriptor.clone()], json!("object"));
			let object_snapshot = exchange_snapshots
				.next()
				.await
				.unwrap()
				.json_deserialize::<ExchangeSnapshot>()
				.data
				.objects
				.into_iter()
				.find(|object| object.descriptor == object_descriptor)
				.unwrap();
			assert_eq!(object_snapshot.cumulative_object_exposer_capacity, 0);
			task.barrier().await;
			while exchange_snapshots
				.next()
				.await
				.unwrap()
				.json_deserialize::<ExchangeSnapshot>()
				.data
				.tags
				.into_iter()
				.find(|tag| tag.descriptor == tag_descriptor)
				.unwrap()
				.cumulative_tag_exposer_capacity
				!= 1
			{
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			while exchange_snapshots
				.next()
				.await
				.unwrap()
				.json_deserialize::<ExchangeSnapshot>()
				.data
				.tags
				.into_iter()
				.find(|tag| tag.descriptor == tag_descriptor)
				.unwrap()
				.cumulative_tag_exposer_capacity
				!= 2
			{
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			while exchange_snapshots
				.next()
				.await
				.unwrap()
				.json_deserialize::<ExchangeSnapshot>()
				.data
				.tags
				.into_iter()
				.find(|tag| tag.descriptor == tag_descriptor)
				.unwrap()
				.cumulative_tag_exposer_capacity
				!= 1
			{
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			while exchange_snapshots
				.next()
				.await
				.unwrap()
				.json_deserialize::<ExchangeSnapshot>()
				.data
				.tags
				.into_iter()
				.find(|tag| tag.descriptor == tag_descriptor)
				.unwrap()
				.cumulative_tag_exposer_capacity
				!= 0
			{
				tokio::time::sleep(Duration::from_millis(10)).await;
			}
			task.barrier().await;
			Ok(())
		})
		.task("regu", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().as_str().to_string() + "#name=regu").as_str())?;
			let _object_expose_contract = hakuban_a.object_observe_contract((vec![json!("tag")], json!("object"))).build();
			task.barrier().await;
			task.barrier().await;
			task.barrier().await;
			task.barrier().await;
			task.barrier().await;
			Ok(())
		})
		.task("riko", |mut task, router| async move {
			let hakuban_a = Exchange::new();
			let upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().to_string() + "#name=riko").as_str())?;
			task.barrier().await;
			let tag_expose_contract_1 = hakuban_a.tag_expose_contract(json!("tag")).build();
			task.barrier().await;
			let _tag_expose_contract_2 = hakuban_a.tag_expose_contract(json!("tag")).build();
			task.barrier().await;
			drop(tag_expose_contract_1);
			task.barrier().await;
			drop(upstream_a);
			task.barrier().await;
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(10000))
		.await
}

#[tokio::test]
async fn router_can_time_out_client() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "timeout=1", true, 0).await;
			Ok(router)
		})
		.task("regu", |mut _task, router| async move {
			let hakuban_a = Exchange::new();
			let upstream_a = WebsocketConnector::new(hakuban_a.clone(), router.url.clone())?;
			tokio::time::sleep(Duration::from_millis(2500)).await;
			assert_eq!(upstream_a.snapshot().upstream_connection.unwrap().upstream_connection_id, 2);
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(4000))
		.await
}

#[tokio::test]
async fn router_doesnt_timeout_if_keep_alive_is_adequate() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "timeout=1", true, 0).await;
			Ok(router)
		})
		.task("regu", |mut _task, router| async move {
			let hakuban_a = Exchange::new();
			let upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().as_str().to_string() + "#keep-alive=250ms").as_str())?;
			tokio::time::sleep(Duration::from_millis(2500)).await;
			assert_eq!(upstream_a.snapshot().upstream_connection.unwrap().upstream_connection_id, 0);
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(4000))
		.await
}

#[tokio::test]
async fn client_can_time_out_router() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "", true, 0).await;
			Ok(router)
		})
		.task("regu", |mut _task, router| async move {
			let hakuban_a = Exchange::new();
			let upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().as_str().to_string() + "#timeout=1s").as_str())?;
			tokio::time::sleep(Duration::from_millis(2500)).await;
			assert_eq!(upstream_a.snapshot().upstream_connection.unwrap().upstream_connection_id, 2);
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(4000))
		.await
}

#[tokio::test]
async fn client_doesnt_timeout_if_keep_alive_is_adequate() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|_task_count, _variant| async {
			let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "keep-alive=250ms", true, 0).await;
			Ok(router)
		})
		.task("regu", |mut _task, router| async move {
			let hakuban_a = Exchange::new();
			let upstream_a = WebsocketConnector::new(hakuban_a.clone(), (router.url.clone().as_str().to_string() + "#timeout=1s").as_str())?;
			while upstream_a.snapshot().upstream_connection.is_none() {
				tokio::time::sleep(Duration::from_millis(100)).await;
			}
			tokio::time::sleep(Duration::from_millis(2500)).await;
			assert_eq!(upstream_a.snapshot().upstream_connection.unwrap().upstream_connection_id, 0);
			Ok(())
		})
		.teardown(|router| async move { router.exit().await })
		.run(Duration::from_millis(4000))
		.await
}

#[tokio::test]
async fn debug_object_can_be_feteched_and_formatted() -> Result<(), Box<dyn Error + Sync + Send>> {
	let _ = env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("hakuban=info")).format_timestamp_millis().try_init();
	Shuffle::new(0, vec!["-"])
		.setup(|task_count, _variant| async move {
			Ok(Topology::StarOfRouters.setup_network(task_count).await)
			//let router = Router::spawn("router", ["--monitor-tag=monitor".to_string()], "", true, 0).await;
			//Ok(router)
		})
		.task("monitor", |mut task, routers| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), (routers[0].url.clone().as_str().to_string() + "#name=monitor").as_str())?;
			let mut exchange_monitor_contract = hakuban_a.object_observe_contract((vec![json!("monitor")], json!("exchange"))).build();
			let mut exchange_snapshots = exchange_monitor_contract.next().await.unwrap();
			let mut listener_monitor_contract = hakuban_a.object_observe_contract((vec![json!("monitor")], json!("websocket-listener"))).build();
			let mut listener_snapshots = listener_monitor_contract.next().await.unwrap();
			let mut connector_monitor_contract = hakuban_a.object_observe_contract((vec![json!("monitor-1")], json!("websocket-connector"))).build();
			let mut connector_snapshots = connector_monitor_contract.next().await.unwrap();
			routers[1].print_all().await;
			task.barrier().await;
			assert!(!format!("{:?}", exchange_snapshots.next().await.unwrap().json_deserialize::<ExchangeSnapshot>().data).is_empty());
			assert!(!format!("{:?}", listener_snapshots.next().await.unwrap().json_deserialize::<WebsocketListenerSnapshot>().data).is_empty());
			assert!(!format!("{:?}", connector_snapshots.next().await.unwrap().json_deserialize::<WebsocketConnectorSnapshot>().data).is_empty());
			task.barrier().await;
			Ok(())
		})
		.task("regu", |mut task, routers| async move {
			let hakuban_a = Exchange::new();
			let _upstream_a = WebsocketConnector::new(hakuban_a.clone(), routers[1].url.clone())?;
			let mut object_observe = hakuban_a.object_observe_contract(([json!({})], json!({}))).build();
			let mut object_state_stream = object_observe.next().await.unwrap();
			let state = object_state_stream.next().await.unwrap().json_deserialize::<String>();
			task.barrier().await;
			assert!(!format!("{:?}", state.format).is_empty());
			assert!(!format!("{:?}", state.version).is_empty());
			assert!(!format!("{:?}", state.synchronized).is_empty());
			task.barrier().await;
			Ok(())
		})
		.task("riko", |mut task, routers| async move {
			let hakuban_b = Exchange::new();
			let _upstream_b = WebsocketConnector::new(hakuban_b.clone(), routers[2].url.clone())?;
			let mut tag_expose = hakuban_b.tag_expose_contract(json!({})).build();
			let mut object_state_sink = tag_expose.next().await.unwrap();
			object_state_sink.next().await.unwrap();
			object_state_sink.send(ObjectState::new("xxx").with_version(vec![1]).json_serialize()).await.unwrap();
			task.barrier().await;
			task.barrier().await;
			Ok(())
		})
		.teardown(Topology::teardown_network)
		.run(Duration::from_millis(4000))
		.await
}
