use std::{
	process::Stdio,
	sync::{
		atomic::{AtomicU16, Ordering},
		Arc,
	},
	time::Duration,
};

use log::debug;
use tokio::{
	io::{AsyncBufReadExt, AsyncWriteExt, BufReader},
	process::Child,
	sync::Mutex,
};
use url::Url;

pub static PORT: AtomicU16 = AtomicU16::new(10000);

pub struct Router {
	process: Mutex<Option<Child>>,
	pub url: Url,
}

impl Router {
	pub async fn spawn(
		name: impl Into<String>,
		extra_args: impl IntoIterator<Item = String>,
		url_params: impl Into<String>,
		debug: bool,
		color: usize,
	) -> Arc<Router> {
		let name = name.into();
		let port = PORT.fetch_add(1, Ordering::Relaxed);
		let address = format!("ws://127.0.0.1:{}#{}", port, url_params.into());

		debug!("Router address: {:?}", &address);

		let mut process = tokio::process::Command::new(format!("target/{}/hakuban-router", if debug { "debug" } else { "release" }))
			.arg("-b")
			.arg(format!("{},name={name}", address.clone()))
			.args(extra_args)
			.stdout(Stdio::piped())
			.stderr(Stdio::piped())
			.stdin(Stdio::piped())
			.kill_on_drop(true)
			.spawn()
			.unwrap(); //FIXME hardcoded path

		let mut reader = BufReader::new(process.stdout.take().unwrap()).lines();
		let name_for_lambda = name.clone();
		tokio::spawn(async move {
			while let Some(line) = reader.next_line().await.unwrap() {
				debug!("\x1b[{}m┃ {} | {}\x1b[K\x1b[0m", 42 + color % 4, name_for_lambda, line);
			}
		});
		let mut reader = BufReader::new(process.stderr.take().unwrap()).lines();
		let name_for_lambda = name.clone();
		tokio::spawn(async move {
			while let Some(line) = reader.next_line().await.unwrap() {
				debug!("\x1b[{}m┃ {} | {}\x1b[K\x1b[0m", 42 + color % 4, name_for_lambda, line);
			}
		});
		loop {
			match tokio::net::TcpStream::connect(format!("127.0.0.1:{}", port)).await {
				Err(_error) => {
					tokio::time::sleep(Duration::from_millis(100)).await;
				}
				Ok(connection) => {
					drop(connection); //.//shutdown(std::net::Shutdown::Both).unwrap();
					break;
				}
			}
		}

		//		Arc::new(Router { process: Mutex::new(Some(process)), url: Url::parse(&address).unwrap() })
		Arc::new(Router { process: Mutex::new(Some(process)), url: Url::parse(&format!("ws://127.0.0.1:{}", port)).unwrap() })
	}

	#[allow(dead_code)]
	pub async fn print_all(&self) {
		if let Some(stdin) = &mut self.process.lock().await.as_mut().unwrap().stdin {
			stdin.write_all("\n".as_ref()).await.unwrap();
			stdin.flush().await.unwrap();
		} else {
			panic!();
		};
	}

	/*
	pub fn rss(&mut self, bytes: usize) -> usize {
		//hardcoded page size
		std::fs::read_to_string(format!("/proc/{}/stat", self.process.id().unwrap())).unwrap().as_str().split(' ').nth(23).unwrap().parse::<usize>().unwrap()
			* 4096
	}
	*/

	//#[allow(dead_code)]
	pub async fn exit(self: Arc<Self>) -> std::io::Result<()> {
		let mut process = self.process.lock().await;
		if let Some(mut process) = process.take() {
			let status = process.try_wait()?;
			if status.is_some() {
				tokio::time::sleep(Duration::from_millis(100)).await;
				panic!("router died prematurely, exit status: {:?}", status);
			}
			nix::sys::signal::kill(nix::unistd::Pid::from_raw(process.id().unwrap() as i32), nix::sys::signal::Signal::SIGTERM).unwrap();
			let status = process.wait().await?;
			if !status.success() {
				tokio::time::sleep(Duration::from_millis(100)).await;
				panic!("router failed, exit status: {:?}", status);
			}
		}
		Ok(())
	}
}
