use std::time::Duration;

#[derive(Clone)]
pub struct ConnectionParameters {
	pub timeout: Duration,
	pub keep_alive: Duration,
	pub nodelay: Option<bool>,
	pub name: Option<String>,
	pub load_limit: Vec<(Duration, u32)>,
}

impl ConnectionParameters {
	pub fn from_url(url: &url::Url) -> Result<ConnectionParameters, String> {
		let mut timeout = Duration::from_secs(20);
		let mut keep_alive = Duration::from_secs(10);
		let mut nodelay = None;
		let mut name = None;
		let mut load_limit = Vec::new();
		if let Some(fragment) = url.fragment() {
			for key_val in fragment.split(',').map(|key_value| key_value.splitn(2, '=').collect::<Vec<&str>>()).filter(|key_value| !key_value.is_empty()) {
				if key_val.len() < 2 {
					if key_val[0].is_empty() {
						continue;
					}
					return Err(format!("{:?} parameter passed without a value", key_val[0]));
				}
				match key_val[0] {
					"timeout" => timeout = duration_str::parse(key_val[1]).map_err(|err| format!("can't parse 'timeout' parameter's value: {}", err))?,
					"keep-alive" => {
						keep_alive = duration_str::parse(key_val[1]).map_err(|err| format!("can't parse 'keep-alive' parameter's value: {}", err))?
					}
					"nodelay" => {
						nodelay = Some(match key_val[1] {
							"true" => true,
							"false" => false,
							_ => return Err("'nodelay' parameter can only be set to 'true' or 'false'".to_string()),
						})
					}
					"name" => name = Some(key_val[1].to_string()),
					"load-limit" => {
						for pair in key_val[1].split("-") {
							if pair.contains(":") {
								let (interval, load) = pair.split_once(":").unwrap();
								let interval = duration_str::parse(interval).map_err(|err| format!("can't parse 'load-limit' interval: {} ", err))?;
								let load =
									load.parse::<u32>().map_err(|_| "can't parse 'load-limit' parameter's value: \"load\" should be an integer".to_string())?;
								load_limit.push((interval, load));
							} else {
								let load =
									pair.parse::<u32>().map_err(|_| "can't parse 'load-limit' parameter's value: \"load\" should be an integer".to_string())?;
								load_limit.push((Duration::from_secs(0), load));
							}
						}
					}
					other => return Err(format!("unknown parameter ('{}') passed in url fragment", other)),
				}
			}
		};
		Ok(ConnectionParameters { timeout, keep_alive, nodelay, name, load_limit })
	}
}
