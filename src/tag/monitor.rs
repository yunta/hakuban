use itertools::Itertools;
use serde::{Deserialize, Serialize};

use crate::TagDescriptor;

#[derive(Serialize, Deserialize)]
pub struct TagSnapshot {
	pub id: u64,
	pub descriptor: TagDescriptor,
	pub objects: Vec<u64>,
	pub tag_observe_contract_count: usize,
	pub tag_expose_contract_count: usize,
	pub upstream_link: Option<TagUpstreamLinkSnapshot>,
	pub downstream_links: Vec<TagDownstreamLinkSnapshot>,
	pub cumulative_tag_exposer_capacity: u64,
}

#[derive(Serialize, Deserialize)]
pub struct TagUpstreamLinkSnapshot {}

#[derive(Serialize, Deserialize)]
pub struct TagDownstreamLinkSnapshot {
	pub downstream_connection_id: u64,
	pub tag_observer: bool,
	pub tag_exposer: bool,
	pub tag_exposer_capacity: u32,
}

impl std::fmt::Debug for TagSnapshot {
	fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		fmt.write_fmt(format_args!("│ Descriptor:   {:?}\n", self.descriptor))?;
		fmt.write_fmt(format_args!("│ Object IDs:   {}\n", self.objects.iter().map(|id| format!("{}", id)).join(", ")))?;
		if self.tag_observe_contract_count > 0 || self.tag_expose_contract_count > 0 {
			fmt.write_fmt(format_args!("│ Contracts:    tag observe: {}  tag expose: {}\n", self.tag_observe_contract_count, self.tag_expose_contract_count))?;
		}
		if let Some(ref _upstream_link) = self.upstream_link {
			fmt.write_fmt(format_args!("│ Upstream connection:    linked\n"))?;
		};
		fmt.write_fmt(format_args!("│ Downstream connections:\n"))?;
		for link in self.downstream_links.iter() {
			fmt.write_fmt(format_args!(
				"│ {:6}    tag observer: {}  tag exposer: {}  capacity: {:6}\n",
				link.downstream_connection_id, link.tag_observer, link.tag_exposer, link.tag_exposer_capacity
			))?;
		}
		Ok(())
	}
}
