use std::sync::Arc;

use super::expose_contract::TagExposeContract;
use crate::{exchange::ExchangeShared, TagDescriptor, TagObserveContract};

//TODO: composition cost, desired update frequency

#[must_use = "Call build() at the end of the contract-build chain."]
pub struct TagObserveContractBuilder {
	exchange_shared: Arc<ExchangeShared>,
	descriptor: TagDescriptor,
}

impl TagObserveContractBuilder {
	pub(crate) fn new(exchange_shared: Arc<ExchangeShared>, descriptor: TagDescriptor) -> TagObserveContractBuilder {
		TagObserveContractBuilder { exchange_shared, descriptor }
	}

	pub fn build(self) -> TagObserveContract {
		let tag_core = self.exchange_shared.tag_acquire(&self.descriptor);
		TagObserveContract::new(tag_core, self.exchange_shared.next_tag_observe_contract_id())
	}
}

#[must_use = "Call build() at the end of the contract-build chain."]
pub struct TagExposeContractBuilder {
	exchange_shared: Arc<ExchangeShared>,
	descriptor: TagDescriptor,
	capacity: Option<u32>,
}

impl TagExposeContractBuilder {
	pub(crate) fn new(exchange_shared: Arc<ExchangeShared>, descriptor: TagDescriptor) -> TagExposeContractBuilder {
		TagExposeContractBuilder { exchange_shared, descriptor, capacity: None }
	}

	pub fn with_capacity(mut self, capacity: u32) -> TagExposeContractBuilder {
		self.capacity = Some(capacity);
		self
	}

	pub fn build(self) -> TagExposeContract {
		let tag_core = self.exchange_shared.tag_acquire(&self.descriptor);
		TagExposeContract::new(tag_core, self.exchange_shared.next_tag_expose_contract_id(), self.capacity.unwrap_or(1))
	}
}
