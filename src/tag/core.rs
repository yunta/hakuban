use std::{
	cmp::min,
	collections::HashMap,
	hash::{Hash, Hasher},
	sync::{Arc, RwLock, RwLockWriteGuard, Weak},
};

use super::{
	monitor::{TagDownstreamLinkSnapshot, TagSnapshot, TagUpstreamLinkSnapshot},
	TagExposeContractId, TagObserveContractId,
};
#[cfg(feature = "downstream")]
use crate::connection::{
	downstream::{DownsteamObjectInterface, DownstreamConnectionId},
	termination::DownstreamBehaviourError,
};
use crate::{
	connection::upstream::{UpstreamConnectionId, UpstreamObjectInterface},
	exchange::ExchangeShared,
	expose_contract::ExposeContractInlet,
	object::core::{Object, ObjectId},
	observe_contract::ObserveContractInlet,
	utils::BoolUtils,
	TagDescriptor,
};

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
pub(crate) struct TagId(pub u64);

pub(crate) struct Tag {
	id: TagId,
	pub(crate) descriptor: TagDescriptor,
	exchange: Arc<ExchangeShared>,
	inner: RwLock<TagInner>,
}

//TODO: rethink and document why tags register their contracts in all their objects
struct TagInner {
	objects: HashMap<ObjectId, Weak<Object>>, // used solely to notify objects of inserted/removed tag contracts
	local_links: LocalLinks,
	upstream_link: Option<UpstreamLink>,
	#[cfg(feature = "downstream")]
	downstream_links: HashMap<DownstreamConnectionId, DownstreamLink>,
	cumulative_capacity: u64,
}

struct LocalLinks {
	tag_observe_contracts: HashMap<TagObserveContractId, ObserveContractInlet>,
	tag_expose_contracts: HashMap<TagExposeContractId, (ExposeContractInlet, u32)>,
}

#[cfg(feature = "downstream")]
struct DownstreamLink {
	object_interface: Arc<DownsteamObjectInterface>,
	tag_observer: bool,
	tag_exposer: bool,
	tag_exposer_capacity: u32,
}

#[cfg(feature = "downstream")]
impl DownstreamLink {
	fn new(object_interface: Arc<DownsteamObjectInterface>) -> DownstreamLink {
		DownstreamLink { object_interface, tag_observer: false, tag_exposer: false, tag_exposer_capacity: 0 }
	}
}

struct UpstreamLink {
	interface: Arc<UpstreamObjectInterface>,
}

impl TagInner {
	//Every user should make sure to update upstream
	fn contribute_capacity(&mut self, capacity: u32) {
		self.cumulative_capacity = self.cumulative_capacity.checked_add(capacity as u64).unwrap();
	}

	fn withdraw_capacity(&mut self, capacity: u32) {
		self.cumulative_capacity = self.cumulative_capacity.checked_sub(capacity as u64).unwrap();
	}
}

impl Tag {
	pub(crate) fn new(
		exchange: Arc<ExchangeShared>,
		id: TagId,
		descriptor: TagDescriptor,
		upstream_connection_send_control_channel_shared: Option<Arc<UpstreamObjectInterface>>,
	) -> Tag {
		Tag {
			id,
			descriptor,
			exchange,
			inner: RwLock::new(TagInner {
				objects: HashMap::new(),
				local_links: LocalLinks { tag_observe_contracts: HashMap::new(), tag_expose_contracts: HashMap::new() },
				#[cfg(feature = "downstream")]
				downstream_links: HashMap::new(),
				upstream_link: upstream_connection_send_control_channel_shared
					.map(|connection_send_control_channel_shared| UpstreamLink { interface: connection_send_control_channel_shared }),
				cumulative_capacity: 0,
			}),
		}
	}

	fn change_links<T>(self: &Arc<Self>, change: impl FnOnce(&mut RwLockWriteGuard<'_, TagInner>, &Vec<Arc<Object>>) -> T) -> T {
		let mut links = self.inner.write().unwrap();
		let existing_objects = links.objects.values().filter_map(Weak::upgrade).collect();
		let ret = change(&mut links, &existing_objects);
		Self::notify_upstream(&mut links, self);
		// dropping links BEFORE existing_objects. drop of existing_objects may cause drop of the object, which in turn will make it call our object_remove, leading to a deadlock if we're still holding on to the links.
		drop(links);
		drop(existing_objects);
		ret
	}

	fn notify_upstream(links: &mut RwLockWriteGuard<'_, TagInner>, tag_core: &Arc<Self>) {
		if let Some(ref link) = links.upstream_link {
			#[cfg(feature = "downstream")]
			link.interface.tag_contracts_changed(
				tag_core.clone(),
				!links.local_links.tag_observe_contracts.is_empty() || links.downstream_links.values().any(|downstream_link| downstream_link.tag_observer),
				!links.local_links.tag_expose_contracts.is_empty() || links.downstream_links.values().any(|downstream_link| downstream_link.tag_exposer),
				min(links.cumulative_capacity, u32::MAX as u64) as u32,
			);
			#[cfg(not(feature = "downstream"))]
			link.interface.tag_contracts_changed(
				tag_core.clone(),
				!links.local_links.tag_observe_contracts.is_empty(),
				!links.local_links.tag_expose_contracts.is_empty(),
				min(links.cumulative_capacity, u32::MAX as u64) as u32,
			);
		};
	}

	//------------------------------- Local Contracts API

	pub(super) fn link_local_tag_observe_contract(self: &Arc<Self>, contract_id: TagObserveContractId, contract: ObserveContractInlet) {
		self.change_links(|mutable, existing_objects| {
			mutable.local_links.tag_observe_contracts.insert(contract_id, contract.clone()).is_none().assert_true();
			existing_objects.iter().for_each(|object| {
				object.link_local_tag_observe_contract(contract_id, contract.clone());
			});
		})
	}

	pub(super) fn link_local_tag_expose_contract(self: &Arc<Self>, contract_id: TagExposeContractId, contract: ExposeContractInlet, capacity: u32) {
		self.change_links(|mutable, existing_objects| {
			mutable.local_links.tag_expose_contracts.insert(contract_id, (contract.clone(), capacity)).is_none().assert_true();
			mutable.contribute_capacity(capacity);
			existing_objects.iter().for_each(|object| {
				object.link_local_tag_expose_contract(contract_id, contract.clone(), capacity);
			});
		})
	}

	pub(super) fn unlink_local_tag_observe_contract(self: &Arc<Self>, contract_id: TagObserveContractId) {
		self.change_links(|mutable, existing_objects| {
			mutable.local_links.tag_observe_contracts.remove(&contract_id).is_some().assert_true();
			existing_objects.iter().for_each(|object| {
				object.unlink_local_tag_observe_contract(contract_id);
			});
		})
	}

	pub(super) fn unlink_local_tag_expose_contract(self: &Arc<Self>, contract_id: TagExposeContractId) {
		self.change_links(|mutable, existing_objects| {
			let removed_link = mutable.local_links.tag_expose_contracts.remove(&contract_id).unwrap();
			mutable.withdraw_capacity(removed_link.1);
			existing_objects.iter().for_each(|object| {
				object.unlink_local_tag_expose_contract(contract_id);
			});
		})
	}

	//------------------------------- Downstream API

	#[cfg(feature = "downstream")]
	pub(crate) fn link_downstream_tag_observe_contract(
		self: &Arc<Self>,
		downstream_connection_id: DownstreamConnectionId,
		downstream_object_interface: Arc<DownsteamObjectInterface>,
	) -> Result<(), DownstreamBehaviourError> {
		self.change_links(|links, existing_objects| {
			// if we see the downstream as already terminated, then either downstream_connection_dropped has already been run (and we don't want to do anything here), or it still will be run (and we don't need to do anything here)
			// if we see the downstream as not terminated, then, knowing that downstream first marks itself as terminated and then runs downstream_connection_dropped, we know that our downstream_connection_dropped has not yet been run (and it's safe to do the usual here)
			if downstream_object_interface.is_terminated() {
				return Ok(());
			};
			let link = links.downstream_links.entry(downstream_connection_id).or_insert_with(|| DownstreamLink::new(downstream_object_interface.clone()));
			if !link.tag_observer && !link.tag_exposer {
				let successfully_registered = link.object_interface.linked_tag_register(self);
				if !successfully_registered {
					links.downstream_links.remove(&downstream_connection_id);
					return Ok(());
				}
			}
			link.tag_observer.true_to_err(DownstreamBehaviourError::DoubleTagObserve)?;
			link.tag_observer = true;
			existing_objects
				.iter()
				.for_each(|object| object.link_downstream_tag_observe_contract(downstream_connection_id, downstream_object_interface.clone()));
			Ok(())
		})
	}

	#[cfg(feature = "downstream")]
	pub(crate) fn link_downstream_tag_expose_contract(
		self: &Arc<Self>,
		downstream_connection_id: DownstreamConnectionId,
		downstream_object_interface: Arc<DownsteamObjectInterface>,
		capacity: u32,
	) -> Result<(), DownstreamBehaviourError> {
		self.change_links(|links, existing_objects| {
			// if we see the downstream as already terminated, then either downstream_connection_dropped has already been run (and we don't want to do anything here), or it still will be run (and we don't need to do anything here)
			// if we see the downstream as not terminated, then, knowing that downstream first marks itself as terminated and then runs downstream_connection_dropped, we know that our downstream_connection_dropped has not yet been run (and it's safe to do the usual here)
			if downstream_object_interface.is_terminated() {
				return Ok(());
			};
			let link = links.downstream_links.entry(downstream_connection_id).or_insert_with(|| DownstreamLink::new(downstream_object_interface.clone()));
			if !link.tag_observer && !link.tag_exposer {
				let successfully_registered = link.object_interface.linked_tag_register(self);
				if !successfully_registered {
					links.downstream_links.remove(&downstream_connection_id);
					return Ok(());
				}
			}
			link.tag_exposer.true_to_err(DownstreamBehaviourError::DoubleTagExpose)?;
			link.tag_exposer = true;
			link.tag_exposer_capacity = capacity;
			links.contribute_capacity(capacity);
			existing_objects
				.iter()
				.for_each(|object| object.link_downstream_tag_expose_contract(downstream_connection_id, downstream_object_interface.clone(), capacity));
			Ok(())
		})
	}

	#[cfg(feature = "downstream")]
	pub(crate) fn unlink_downstream_tag_observe_contract(
		self: &Arc<Self>,
		downstream_connection_id: DownstreamConnectionId,
	) -> Result<(), DownstreamBehaviourError> {
		self.change_links(|links, existing_objects| {
			let Some(link) = links.downstream_links.get_mut(&downstream_connection_id) else { return Ok(()) };
			if !link.tag_exposer && link.tag_observer {
				link.object_interface.linked_tag_unregister(self)
			}
			link.tag_observer.false_to_err(DownstreamBehaviourError::TagUnobserveWithoutMatchingObserve)?;
			link.tag_observer = false;
			if !link.tag_observer && !link.tag_exposer {
				links.downstream_links.remove(&downstream_connection_id);
			}
			existing_objects.iter().for_each(|object| object.unlink_downstream_tag_observe_contract(downstream_connection_id));
			Ok(())
		})
	}

	#[cfg(feature = "downstream")]
	pub(crate) fn unlink_downstream_tag_expose_contract(
		self: &Arc<Self>,
		downstream_connection_id: DownstreamConnectionId,
	) -> Result<(), DownstreamBehaviourError> {
		self.change_links(|links, existing_objects| {
			let Some(link) = links.downstream_links.get_mut(&downstream_connection_id) else { return Ok(()) };
			if link.tag_exposer && !link.tag_observer {
				link.object_interface.linked_tag_unregister(self)
			}
			link.tag_exposer.false_to_err(DownstreamBehaviourError::TagUexposeWithoutMatchingExpose)?;
			link.tag_exposer = false;
			let capacity = link.tag_exposer_capacity;
			link.tag_exposer_capacity = 0;
			if !link.tag_observer && !link.tag_exposer {
				links.downstream_links.remove(&downstream_connection_id);
			}
			links.withdraw_capacity(capacity);
			existing_objects.iter().for_each(|object| object.unlink_downstream_tag_expose_contract(downstream_connection_id, capacity));
			Ok(())
		})
	}

	#[cfg(feature = "downstream")]
	pub(crate) fn set_downstream_tag_expose_capacity(
		self: &Arc<Self>,
		downstream_connection_id: DownstreamConnectionId,
		capacity: u32,
	) -> Result<(), DownstreamBehaviourError> {
		self.change_links(|links, existing_objects| {
			let Some(link) = links.downstream_links.get_mut(&downstream_connection_id) else { return Ok(()) };
			link.tag_exposer.false_to_err(DownstreamBehaviourError::ChangeExposeCapacityWhileNotExposing)?;
			let withdraw_capacity = link.tag_exposer_capacity;
			link.tag_exposer_capacity = capacity;
			links.withdraw_capacity(withdraw_capacity);
			links.contribute_capacity(capacity);
			existing_objects.iter().for_each(|object| {
				object.change_downstream_tag_expose_capacity(downstream_connection_id, withdraw_capacity, capacity).ok();
			});
			//TODO: maybe load-balance here?
			Ok(())
		})
	}

	#[cfg(feature = "downstream")]
	pub(crate) fn downstream_connection_dropped(self: &Arc<Self>, downstream_connection_id: DownstreamConnectionId) {
		self.change_links(|links, existing_objects| {
			let link = links.downstream_links.remove(&downstream_connection_id).unwrap();
			links.withdraw_capacity(link.tag_exposer_capacity);
			existing_objects.iter().for_each(|object| object.downstream_connection_dropped(downstream_connection_id));
		});
	}

	//------------------------------- Upstream API

	pub(crate) fn set_upstream_connection(self: &Arc<Self>, new_upstream_connection: Arc<UpstreamObjectInterface>) {
		let mut links = self.inner.write().unwrap();
		if links.upstream_link.is_some() && links.upstream_link.as_ref().unwrap().interface.id.0 < new_upstream_connection.id.0 {
			links.upstream_link.take().unwrap();
		}
		if links.upstream_link.is_none() {
			#[cfg(feature = "downstream")]
			new_upstream_connection.tag_contracts_changed(
				self.clone(),
				!links.local_links.tag_observe_contracts.is_empty() || links.downstream_links.values().any(|downstream_link| downstream_link.tag_observer),
				!links.local_links.tag_expose_contracts.is_empty() || links.downstream_links.values().any(|downstream_link| downstream_link.tag_exposer),
				min(links.cumulative_capacity, u32::MAX as u64) as u32,
			);
			#[cfg(not(feature = "downstream"))]
			new_upstream_connection.tag_contracts_changed(
				self.clone(),
				!links.local_links.tag_observe_contracts.is_empty(),
				!links.local_links.tag_expose_contracts.is_empty(),
				min(links.cumulative_capacity, u32::MAX as u64) as u32,
			);
			links.upstream_link.is_none().assert_true();
			links.upstream_link = Some(UpstreamLink { interface: new_upstream_connection });
		}
	}

	pub(crate) fn remove_upstream_connection(self: &Arc<Self>, remove_upstream_connection_id: UpstreamConnectionId) {
		let mut links = self.inner.write().unwrap();
		if links.upstream_link.is_some() && links.upstream_link.as_ref().unwrap().interface.id.0 <= remove_upstream_connection_id.0 {
			links.upstream_link.take().unwrap();
		}
	}

	//------------------------------- Object API

	pub(crate) fn object_insert(&self, object_core: &Arc<Object>) {
		let mut links = self.inner.write().unwrap();
		links.objects.insert(object_core.id(), Arc::downgrade(object_core));
		for (contract_id, tag_observe_contract) in &links.local_links.tag_observe_contracts {
			object_core.link_local_tag_observe_contract(*contract_id, tag_observe_contract.clone());
		}
		for (contract_id, (tag_expose_contract, composition_cost)) in &links.local_links.tag_expose_contracts {
			object_core.link_local_tag_expose_contract(*contract_id, tag_expose_contract.clone(), *composition_cost);
		}
		#[cfg(feature = "downstream")]
		for (downstream_connection_id, link) in &links.downstream_links {
			if link.tag_observer {
				object_core.link_downstream_tag_observe_contract(*downstream_connection_id, link.object_interface.clone());
			}
			if link.tag_exposer {
				object_core.link_downstream_tag_expose_contract(*downstream_connection_id, link.object_interface.clone(), link.tag_exposer_capacity);
			}
		}
	}

	pub(crate) fn object_remove(&self, object_id: ObjectId) {
		let mut links = self.inner.write().unwrap();
		links.objects.remove(&object_id);
	}

	pub(crate) fn snapshot(&self) -> TagSnapshot {
		let links = self.inner.read().unwrap();
		#[cfg(feature = "downstream")]
		let mut downstream_links: Vec<TagDownstreamLinkSnapshot> = links
			.downstream_links
			.iter()
			.map(|(id, link)| TagDownstreamLinkSnapshot {
				downstream_connection_id: id.0,
				tag_observer: link.tag_observer,
				tag_exposer: link.tag_exposer,
				tag_exposer_capacity: link.tag_exposer_capacity,
			})
			.collect();
		#[cfg(not(feature = "downstream"))]
		let mut downstream_links: Vec<TagDownstreamLinkSnapshot> = vec![];

		downstream_links.sort_by_key(|link| link.downstream_connection_id);
		let mut objects: Vec<u64> = links.objects.keys().map(|id| id.0).collect();
		objects.sort();
		TagSnapshot {
			id: self.id.0,
			descriptor: self.descriptor.clone(),
			objects,
			tag_observe_contract_count: links.local_links.tag_observe_contracts.len(),
			tag_expose_contract_count: links.local_links.tag_expose_contracts.len(),
			upstream_link: links.upstream_link.as_ref().map(|_link| TagUpstreamLinkSnapshot {}),
			downstream_links,
			cumulative_tag_exposer_capacity: links.cumulative_capacity,
		}
	}
}

//TEST-NEEDED: removing this doesn't make any test fail

impl Drop for Tag {
	fn drop(&mut self) {
		self.exchange.tag_release(&self.descriptor);
	}
}

impl Hash for Tag {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.id.hash(state);
	}
}

impl PartialEq for Tag {
	fn eq(&self, other: &Self) -> bool {
		self.id == other.id
	}
}

impl Eq for Tag {}
