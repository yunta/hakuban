/*! Convenience functions to easily serialize and deserialize [ObjectState](crate::ObjectState)s
*/

pub(crate) mod json;

pub use json::*;
