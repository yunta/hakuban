use std::sync::Arc;

use crate::{
	descriptor::ObjectDescriptor,
	exchange::ExchangeShared,
	object::{expose_contract::ObjectExposeContract, observe_contract::ObjectObserveContract},
};

//TODO: desired update frequency and other params

#[must_use = "Call .build() at the end of the contract-build chain."]
pub struct ObjectObserveContractBuilder {
	exchange_shared: Arc<ExchangeShared>,
	descriptor: ObjectDescriptor,
}

impl ObjectObserveContractBuilder {
	pub(crate) fn new(exchange_shared: Arc<ExchangeShared>, descriptor: ObjectDescriptor) -> ObjectObserveContractBuilder {
		ObjectObserveContractBuilder { exchange_shared, descriptor }
	}

	pub fn build(self) -> ObjectObserveContract {
		let object_core = self.exchange_shared.object_acquire(&self.descriptor);
		ObjectObserveContract::new(object_core, self.exchange_shared.next_object_observe_contract_id())
	}
}

#[must_use = "Call .build() at the end of the contract-build chain."]
pub struct ObjectExposeContractBuilder {
	exchange_shared: Arc<ExchangeShared>,
	descriptor: ObjectDescriptor,
	capacity: Option<u32>,
}

impl ObjectExposeContractBuilder {
	pub(crate) fn new(exchange_shared: Arc<ExchangeShared>, descriptor: ObjectDescriptor) -> ObjectExposeContractBuilder {
		ObjectExposeContractBuilder { exchange_shared, descriptor, capacity: None }
	}

	pub fn with_capacity(mut self, capacity: u32) -> ObjectExposeContractBuilder {
		self.capacity = Some(capacity);
		self
	}

	pub fn build(self) -> ObjectExposeContract {
		let object_core = self.exchange_shared.object_acquire(&self.descriptor);
		ObjectExposeContract::new(object_core, self.exchange_shared.next_object_expose_contract_id(), self.capacity.unwrap_or(1))
	}
}
