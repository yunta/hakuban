use std::sync::{Arc, Mutex};

use super::{core::Object, state_stream::ObjectStateStream};
use crate::{
	observe_contract::{ObserveContractInlet, ObserveContractShared},
	ObjectDescriptor,
};

#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug)]
pub(crate) struct ObjectObserveContractId(pub u64);

/// Represents a wish, a __contract__ to observe an object
///
/// ObjectObserveContract is a [futures::stream::Stream], emitting [ObjectStateStream] objects.
///
/// ObjectObserveContract will not emit more than one [ObjectStateStream] at a time. New [ObjectStateStream] will get emitted only when previous one gets dropped.
///
/// The [ObjectStateStream] will end when contract gets dropped.
pub struct ObjectObserveContract {
	id: ObjectObserveContractId,
	object_core: Arc<Object>,
	shared: Arc<Mutex<ObserveContractShared>>,
}

impl ObjectObserveContract {
	pub(crate) fn new(object_core: Arc<Object>, id: ObjectObserveContractId) -> ObjectObserveContract {
		let shared = Arc::new(Mutex::new(ObserveContractShared::new()));
		object_core.link_local_object_observe_contract(id, ObserveContractInlet::new(shared.clone()));
		ObjectObserveContract { id, object_core, shared }
	}

	pub fn descriptor(&self) -> &ObjectDescriptor {
		&self.object_core.descriptor
	}
}

impl Drop for ObjectObserveContract {
	fn drop(&mut self) {
		self.object_core.unlink_local_object_observe_contract(self.id);
	}
}

impl futures::Stream for ObjectObserveContract {
	type Item = ObjectStateStream;

	fn poll_next(self: std::pin::Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> std::task::Poll<Option<Self::Item>> {
		self.shared.lock().unwrap().poll_next(cx)
	}
}

impl std::fmt::Debug for ObjectObserveContract {
	fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		formatter.write_str(&format!("ObjectObserveContract:{}", self.object_core.descriptor))
	}
}
