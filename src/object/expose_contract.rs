use std::sync::{Arc, Mutex};

use serde::Serialize;

use super::{core::Object, state_sink::ObjectStateSink};
use crate::{
	expose_contract::{ExposeContractInlet, ExposeContractShared},
	ObjectDescriptor,
};

#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug, Serialize)]
pub(crate) struct ObjectExposeContractId(pub u64);

/// Represents a wish, a __contract__ to expose an object
///
/// ObjectExposeContract is a [futures::stream::Stream], emitting [ObjectStateSink] stucts.
///
/// ObjectStateSink will only get emitted if/when [Exchange](crate::Exchange) gets to know at least one observer ([ObjectObserveContract](crate::ObjectObserveContract) or [TagObserveContract](crate::TagObserveContract)) of the object the contract pertains to.
/// ObjectExposeContract will not emit more than one [ObjectStateSink] at a time. New [ObjectStateSink] will get emitted only when previous one gets dropped.
///
/// The stream part of the [ObjectStateSink] will end when contract gets dropped.
#[derive(Clone)]
pub struct ObjectExposeContract {
	id: ObjectExposeContractId,
	object_core: Arc<Object>,
	shared: Arc<Mutex<ExposeContractShared>>,
}

impl ObjectExposeContract {
	pub(crate) fn new(object_core: Arc<Object>, id: ObjectExposeContractId, capacity: u32) -> ObjectExposeContract {
		let shared = Arc::new(Mutex::new(ExposeContractShared::new()));
		object_core.link_local_object_expose_contract(id, ExposeContractInlet::new(shared.clone()), capacity);
		ObjectExposeContract { id, object_core, shared }
	}

	pub fn descriptor(&self) -> &ObjectDescriptor {
		&self.object_core.descriptor
	}
}

impl Drop for ObjectExposeContract {
	fn drop(&mut self) {
		self.object_core.unlink_local_object_expose_contract(self.id);
	}
}

impl futures::Stream for ObjectExposeContract {
	type Item = ObjectStateSink;

	fn poll_next(self: std::pin::Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> std::task::Poll<Option<Self::Item>> {
		self.shared.lock().unwrap().poll_next(cx)
	}
}

impl std::fmt::Debug for ObjectExposeContract {
	fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		formatter.write_str(&format!("ObjectExposeContract:{}", self.object_core.descriptor))
	}
}
