use serde::{Deserialize, Serialize};

use super::{DataFormat, DataVersion};
use crate::ObjectDescriptor;

#[derive(Serialize, Deserialize)]
pub struct ObjectSnapshot {
	pub id: u64,
	pub descriptor: ObjectDescriptor,
	pub should_exist: bool,
	pub data_needed: bool,
	pub assigned_exposer: Option<ObjectStateSinkOwnerSnapshot>,
	pub object_observe_contract_count: usize,
	pub object_expose_contract_count: usize,
	pub tag_observe_contract_count: usize,
	pub tag_expose_contract_count: usize,
	pub upstream_link: Option<ObjectUpstreamLinkSnapshot>,
	pub downstream_links: Vec<ObjectDownstreamLinkSnapshot>,
	pub cumulative_object_exposer_capacity: u64,
	pub data_state: Option<ObjectStateSnapshot>,
}

#[derive(Serialize, Deserialize)]
pub struct ObjectStateSnapshot {
	pub version: DataVersion,
	pub format: DataFormat,
	pub size: usize,
	pub synchronized_ms_ago: u64,
}

#[derive(Serialize, Deserialize)]
pub struct ObjectUpstreamLinkSnapshot {
	pub sinks_receiving_from_upstream_count: usize,
	pub streams_sending_to_upstream_count: usize,
}

#[derive(Serialize, Deserialize)]
pub struct ObjectDownstreamLinkSnapshot {
	pub downstream_connection_id: u64,
	pub object_observer: bool,
	pub object_exposer: bool,
	pub object_exposer_capacity: u32,
	pub tag_observer_count: usize,
	pub tag_exposer_count: usize,
	pub stream: bool,
	pub sink: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum ObjectStateSinkOwnerSnapshot {
	ObjectExposeContract(u64),
	TagExposeContract(u64),
	UpstreamConnection(u64),
	DownstreamConnection(u64),
}

impl std::fmt::Debug for ObjectSnapshot {
	fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		fmt.write_fmt(format_args!("│ Descriptor:   {:?}\n", self.descriptor))?;
		fmt.write_fmt(format_args!("│ Status:       should exist: {:?}  data needed: {:?}\n", self.should_exist, self.data_needed))?;
		if let Some(ref assigned_exposer) = self.assigned_exposer {
			fmt.write_fmt(format_args!("│ Assigned exposer:       {:?}\n", assigned_exposer))?;
		}
		fmt.write_fmt(format_args!("│ Cumulative object exposer capacity:    {:?}\n", self.cumulative_object_exposer_capacity))?;
		if self.object_observe_contract_count > 0
			|| self.object_expose_contract_count > 0
			|| self.tag_observe_contract_count > 0
			|| self.tag_expose_contract_count > 0
		{
			fmt.write_fmt(format_args!(
				"│ Contracts:    object observe: {}  object expose: {}  tag observe: {}  tag expose: {}\n",
				self.object_observe_contract_count, self.object_expose_contract_count, self.tag_observe_contract_count, self.tag_expose_contract_count
			))?;
		}
		if let Some(ref upstream_link) = self.upstream_link {
			fmt.write_fmt(format_args!(
				"│ Upstream connection:    sinking from upstream: {}  streaming to upstream: {}\n",
				upstream_link.sinks_receiving_from_upstream_count, upstream_link.streams_sending_to_upstream_count
			))?;
		};
		fmt.write_fmt(format_args!("│ Downstream connections:\n"))?;
		for link in self.downstream_links.iter() {
			fmt.write_fmt(format_args!(
				"│ {:6}    object observer: {:5}  object exposer: {:5}  tag observer: {}  tag exposer: {}  stream: {:5}  sink: {:5}  capacity: {:6}\n",
				link.downstream_connection_id,
				link.object_observer,
				link.object_exposer,
				link.tag_observer_count,
				link.tag_exposer_count,
				link.stream,
				link.sink,
				link.object_exposer_capacity
			))?;
		}
		if let Some(ref state) = self.data_state {
			fmt.write_fmt(format_args!("│ Data: {:?} {:?} {:?}B {:?}\n", state.version, state.format, state.size, state.synchronized_ms_ago))?;
		}
		Ok(())
	}
}
