#![allow(clippy::not_unsafe_ptr_arg_deref)]

use log::{trace, Level};
use wasm_bindgen::prelude::*;

use super::wasm_result::{WasmResult, WasmResultStatus};

#[cfg(feature = "console_error_panic_hook")]
extern crate console_error_panic_hook;

#[wasm_bindgen]
pub fn hakuban_logger_initialize(default_log_level: String) -> WasmResult {
	#[cfg(feature = "console_error_panic_hook")]
	console_error_panic_hook::set_once();

	if let Err(error) = console_log::init_with_level(match default_log_level.as_str() {
		"error" => Level::Error,
		"warn" => Level::Warn,
		"info" => Level::Info,
		"debug" => Level::Debug,
		"trace" => Level::Trace,
		other => {
			return WasmResult::error(
				WasmResultStatus::InvalidLogLevel,
				format!("Invalid log level: {:?}. Valid levels are: error, warn, info, debug, trace.", other),
			);
		}
	}) {
		WasmResult::error(WasmResultStatus::LoggerInitializationError, error)
	} else {
		trace!("Logger initialized");
		WasmResult::ok()
	}
}
