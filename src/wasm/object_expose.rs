#![allow(clippy::not_unsafe_ptr_arg_deref)]

use futures::{FutureExt, StreamExt};
use wasm_bindgen::prelude::*;

use super::{WasmExchange, WasmFuture, WasmResult};
use crate::{ObjectDescriptor, ObjectExposeContract};

#[wasm_bindgen]
pub fn hakuban_object_expose_contract_new(exchange: *mut WasmExchange, descriptor: *mut ObjectDescriptor, capacity: u32) -> *mut ObjectExposeContract {
	let exchange = unsafe { exchange.as_mut().unwrap() };
	let descriptor = unsafe { descriptor.as_mut().unwrap() };
	Box::into_raw(Box::new(exchange.exchange.object_expose_contract(descriptor.clone()).with_capacity(capacity).build()))
}

#[wasm_bindgen]
pub fn hakuban_object_expose_contract_drop(object_ptr: *mut ObjectExposeContract) {
	drop(unsafe { Box::from_raw(object_ptr) });
}

#[wasm_bindgen]
pub fn hakuban_object_expose_contract_next(object_expose_pointer: *mut ObjectExposeContract) -> *mut WasmFuture {
	let object_expose: &mut ObjectExposeContract = unsafe { object_expose_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(WasmFuture::new(object_expose.next().map(|item| item.map(WasmResult::pointer).unwrap_or(WasmResult::end_of_stream())))))
}
