#![allow(clippy::not_unsafe_ptr_arg_deref)]

use futures::{FutureExt, StreamExt};
use wasm_bindgen::prelude::*;

use super::{WasmExchange, WasmFuture, WasmResult};
use crate::{TagDescriptor, TagExposeContract};

#[wasm_bindgen]
pub fn hakuban_tag_expose_contract_new(exchange: *mut WasmExchange, descriptor: *mut TagDescriptor, capacity: u32) -> *mut TagExposeContract {
	let exchange = unsafe { exchange.as_mut().unwrap() };
	let descriptor = unsafe { descriptor.as_mut().unwrap() };
	Box::into_raw(Box::new(exchange.exchange.tag_expose_contract(descriptor.clone()).with_capacity(capacity).build()))
}

#[wasm_bindgen]
pub fn hakuban_tag_expose_contract_drop(tag_expose_contract_pointer: *mut TagExposeContract) {
	drop(unsafe { Box::from_raw(tag_expose_contract_pointer) });
}

#[wasm_bindgen]
pub fn hakuban_tag_expose_contract_next(tag_expose_pointer: *mut TagExposeContract) -> *mut WasmFuture {
	let tag_expose = unsafe { tag_expose_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(WasmFuture::new(tag_expose.next().map(|item| item.map(WasmResult::pointer).unwrap_or(WasmResult::end_of_stream())))))
}
