#![allow(clippy::not_unsafe_ptr_arg_deref)]

use futures::{FutureExt, StreamExt};
use wasm_bindgen::prelude::*;

use super::{WasmFuture, WasmResult};
use crate::{object::ObjectStateStream, ObjectDescriptor};

#[wasm_bindgen]
pub fn hakuban_object_state_stream_drop(object_state_stream_pointer: *mut ObjectStateStream) {
	drop(unsafe { Box::from_raw(object_state_stream_pointer) });
}

#[wasm_bindgen]
pub fn hakuban_object_state_stream_next(object_state_stream_pointer: *mut ObjectStateStream) -> *mut WasmFuture {
	let object_state_stream: &mut ObjectStateStream = unsafe { object_state_stream_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(WasmFuture::new(object_state_stream.next().map(|item| item.map(WasmResult::pointer).unwrap_or(WasmResult::end_of_stream())))))
}

#[wasm_bindgen]
pub fn hakuban_object_state_stream_descriptor(object_state_stream_pointer: *mut ObjectStateStream) -> *mut ObjectDescriptor {
	let object_state_stream = unsafe { object_state_stream_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(object_state_stream.descriptor().clone()))
}
