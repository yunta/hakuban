#![allow(clippy::not_unsafe_ptr_arg_deref)]

use std::sync::Arc;

use wasm_bindgen::prelude::*;

use super::WasmResult;
use crate::{object::ObjectState, DataBytes, DataSynchronized};

#[wasm_bindgen]
pub fn hakuban_object_state_new(version: Box<[i64]>, format_jsvalues: Box<[JsValue]>, data: Box<[u8]>, synchronized_us_ago: u64) -> WasmResult {
	let mut format: Vec<String> = vec![];
	for entry in format_jsvalues.iter() {
		if let Some(format_element) = entry.as_string() {
			format.push(format_element);
		} else {
			return WasmResult::error(super::WasmResultStatus::InvalidString, "Value submitted as part of format array is not a string.");
		}
	}

	let serialized_data: Arc<Vec<u8>> = Arc::new(data.into());
	let synchronized = DataSynchronized::from_micros_ago(synchronized_us_ago);
	let object_state = ObjectState { data: serialized_data, version: version.into(), format, synchronized };

	WasmResult::pointer(object_state)
}

#[wasm_bindgen]
pub fn hakuban_object_state_drop(object_state_pointer: *mut ObjectState<DataBytes>) {
	drop(unsafe { Box::from_raw(object_state_pointer) });
}

#[wasm_bindgen]
pub fn hakuban_object_state_version(object_state_pointer: *mut ObjectState<DataBytes>) -> Box<[i64]> {
	let object_state = unsafe { object_state_pointer.as_mut().unwrap() };
	object_state.version.clone().into()
}

#[wasm_bindgen]
pub fn hakuban_object_state_synchronized_ago(object_state_pointer: *mut ObjectState<DataBytes>) -> u64 {
	let object_state = unsafe { object_state_pointer.as_mut().unwrap() };
	object_state.synchronized.micros_ago()
}

#[wasm_bindgen]
pub fn hakuban_object_state_format(object_state_pointer: *mut ObjectState<DataBytes>) -> Box<[JsValue]> {
	let object_state = unsafe { object_state_pointer.as_mut().unwrap() };
	object_state.format.clone().into_iter().map(JsValue::from).collect()
}

#[wasm_bindgen]
pub fn hakuban_object_state_data(object_state_pointer: *mut ObjectState<DataBytes>) -> Box<[u8]> {
	let object_state = unsafe { object_state_pointer.as_mut().unwrap() };
	(*object_state.data).clone().into_boxed_slice()
}
