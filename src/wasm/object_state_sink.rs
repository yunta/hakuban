#![allow(clippy::not_unsafe_ptr_arg_deref)]

use futures::{stream::StreamExt, FutureExt, SinkExt};
use wasm_bindgen::prelude::*;

use super::{WasmFuture, WasmResult};
use crate::{
	object::{
		state_sink::{ObjectStateSink, ObjectStateSinkParams},
		ObjectState,
	},
	DataBytes, ObjectDescriptor,
};

#[wasm_bindgen]
pub fn hakuban_object_state_sink_descriptor(object_state_sink_pointer: *mut ObjectStateSink) -> *mut ObjectDescriptor {
	let object_state_sink: &mut ObjectStateSink = unsafe { object_state_sink_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(object_state_sink.descriptor().clone()))
}

#[wasm_bindgen]
pub fn hakuban_object_state_sink_drop(object_state_sink_pointer: *mut ObjectStateSink) {
	drop(unsafe { Box::from_raw(object_state_sink_pointer) });
}

#[wasm_bindgen]
pub fn hakuban_object_state_sink_next(object_state_sink_pointer: *mut ObjectStateSink) -> *mut WasmFuture {
	let object_state_sink: &mut ObjectStateSink = unsafe { object_state_sink_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(WasmFuture::new(object_state_sink.next().map(|item| item.map(WasmResult::pointer).unwrap_or(WasmResult::end_of_stream())))))
}

#[wasm_bindgen]
pub fn hakuban_object_state_sink_push(object_state_sink_pointer: *mut ObjectStateSink, object_state_pointer: *mut ObjectState<DataBytes>) -> *mut WasmFuture {
	let object_state_sink: &mut ObjectStateSink = unsafe { object_state_sink_pointer.as_mut().unwrap() };
	let object_state: &ObjectState<DataBytes> = unsafe { object_state_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(WasmFuture::new(object_state_sink.send(object_state.clone()).map(|result| match result {
		Ok(()) => WasmResult::ok(),
		Err(()) => panic!(),
	}))))
}

#[wasm_bindgen]
pub fn hakuban_object_state_sink_params_drop(object_state_sink_params_pointer: *mut ObjectStateSinkParams) {
	drop(unsafe { Box::from_raw(object_state_sink_params_pointer) });
}
