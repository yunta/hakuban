#![allow(clippy::not_unsafe_ptr_arg_deref)]

use futures::{FutureExt, StreamExt};
use wasm_bindgen::prelude::*;

use super::{wasm_future::WasmFuture, WasmExchange, WasmResult};
use crate::{TagDescriptor, TagObserveContract};

#[wasm_bindgen]
pub fn hakuban_tag_observe_contract_new(exchange: *mut WasmExchange, descriptor: *mut TagDescriptor) -> *mut TagObserveContract {
	let exchange = unsafe { exchange.as_mut().unwrap() };
	let descriptor = unsafe { descriptor.as_mut().unwrap() };
	Box::into_raw(Box::new(exchange.exchange.tag_observe_contract(descriptor.clone()).build()))
}

#[wasm_bindgen]
pub fn hakuban_tag_observe_contract_drop(tag_pointer: *mut TagObserveContract) {
	drop(unsafe { Box::from_raw(tag_pointer) });
}

#[wasm_bindgen]
pub fn hakuban_tag_observe_contract_next(tag_observe_pointer: *mut TagObserveContract) -> *mut WasmFuture {
	let tag_observe = unsafe { tag_observe_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(WasmFuture::new(tag_observe.next().map(|item| item.map(WasmResult::pointer).unwrap_or(WasmResult::end_of_stream())))))
}
