#![allow(clippy::not_unsafe_ptr_arg_deref)]

use futures::{FutureExt, StreamExt};
use wasm_bindgen::prelude::*;

use super::{wasm_future::WasmFuture, WasmExchange, WasmResult};
use crate::{ObjectDescriptor, ObjectObserveContract};

#[wasm_bindgen]
pub fn hakuban_object_observe_contract_new(exchange: *mut WasmExchange, descriptor: *mut ObjectDescriptor) -> *mut ObjectObserveContract {
	let exchange = unsafe { exchange.as_mut().unwrap() };
	let descriptor = unsafe { descriptor.as_mut().unwrap() };
	Box::into_raw(Box::new(exchange.exchange.object_observe_contract(descriptor.clone()).build()))
}

#[wasm_bindgen]
pub fn hakuban_object_observe_contract_drop(object_ptr: *mut ObjectObserveContract) {
	drop(unsafe { Box::from_raw(object_ptr) });
}

#[wasm_bindgen]
pub fn hakuban_object_observe_contract_next(object_observe_pointer: *mut ObjectObserveContract) -> *mut WasmFuture {
	let object_observe: &mut ObjectObserveContract = unsafe { object_observe_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(WasmFuture::new(object_observe.next().map(|item| item.map(WasmResult::pointer).unwrap_or(WasmResult::end_of_stream())))))
}
