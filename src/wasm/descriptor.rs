#![allow(clippy::not_unsafe_ptr_arg_deref)]

use wasm_bindgen::prelude::*;

use super::wasm_result::WasmResult;
use crate::{wasm::wasm_result::WasmResultStatus, ObjectDescriptor, TagDescriptor};

#[wasm_bindgen]
pub fn hakuban_tag_descriptor_new(json_string: String) -> WasmResult {
	match serde_json::from_str::<serde_json::Value>(json_string.as_str()) {
		Ok(json) => WasmResult::pointer(TagDescriptor::new(json)),
		Err(error) => WasmResult::error(WasmResultStatus::InvalidJSON, error),
	}
}

#[wasm_bindgen]
pub fn hakuban_tag_descriptor_drop(descriptor_pointer: *mut TagDescriptor) {
	drop(unsafe { Box::from_raw(descriptor_pointer) });
}

#[wasm_bindgen]
pub fn hakuban_tag_descriptor_json(descriptor_pointer: *mut TagDescriptor) -> String {
	let descriptor = unsafe { descriptor_pointer.as_mut().unwrap() };
	descriptor.json.to_string()
}

#[wasm_bindgen]
pub fn hakuban_object_descriptor_new(json_string: String, tag_pointers: &[u32]) -> WasmResult {
	let json = match serde_json::from_str::<serde_json::Value>(json_string.as_str()) {
		Ok(json) => json,
		Err(error) => {
			return WasmResult::error(WasmResultStatus::InvalidJSON, error);
		}
	};
	let tags: Vec<TagDescriptor> = tag_pointers.iter().map(|number| unsafe { (*number as *mut TagDescriptor).as_mut() }.unwrap().clone()).collect();
	WasmResult::pointer(ObjectDescriptor::new(tags, json))
}

#[wasm_bindgen]
pub fn hakuban_object_descriptor_drop(descriptor_pointer: *mut ObjectDescriptor) {
	drop(unsafe { Box::from_raw(descriptor_pointer) });
}

#[wasm_bindgen]
pub fn hakuban_object_descriptor_json(descriptor_pointer: *mut ObjectDescriptor) -> String {
	let descriptor = unsafe { descriptor_pointer.as_mut().unwrap() };
	descriptor.json.to_string()
}

#[wasm_bindgen]
pub fn hakuban_object_descriptor_tags(descriptor_pointer: *mut ObjectDescriptor) -> Box<[u32]> {
	let descriptor = unsafe { descriptor_pointer.as_mut().unwrap() };
	descriptor.tags.iter().map(|tag| Box::into_raw(Box::new(tag.clone())) as u32).collect::<Vec<u32>>().into_boxed_slice()
}
