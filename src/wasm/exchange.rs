#![allow(clippy::not_unsafe_ptr_arg_deref)]

use std::sync::{Arc, Mutex};

use log::trace;
use wasm_bindgen::prelude::*;

use super::wasm_result::WasmResult;
use crate::Exchange;

pub struct WasmExchange {
	pub(crate) exchange: Exchange,
	pub(crate) wakers_triggered: Arc<Mutex<Vec<u64>>>,
}

#[wasm_bindgen]
pub fn hakuban_exchange_new() -> WasmResult {
	trace!("Constructing exchange");
	WasmResult::pointer(WasmExchange { exchange: Exchange::new(), wakers_triggered: Arc::new(Mutex::new(Vec::new())) })
}

#[wasm_bindgen]
pub fn hakuban_exchange_drop(exchange: *mut WasmExchange) {
	let exchange = unsafe { Box::from_raw(exchange) };
	trace!("Dropping exchange");
	drop(exchange);
}

#[wasm_bindgen]
pub fn hakuban_exchange_notified(exchange: *mut WasmExchange) -> Box<[u64]> {
	let exchange = unsafe { exchange.as_mut().unwrap() };
	let mut wakers_triggered = exchange.wakers_triggered.lock().unwrap();
	let notified: Vec<u64> = wakers_triggered.drain(..).collect();
	//trace!("waking {:?}", notified);
	notified.into()
}
