use std::fmt::Debug;

use wasm_bindgen::prelude::*;

#[derive(Debug, Clone, Copy)]
#[wasm_bindgen]
pub enum WasmResultStatus {
	Ok = 0,
	Pointer = 1,
	Pending = 2,
	EndOfStream = 3,
	InvalidString = 4,
	InvalidJSON = 5,
	InvalidURL = 6,
	InvalidLogLevel = 7,
	LoggerInitializationError = 8,
	ConnectionTerminated = 9,
}

#[derive(Debug)]
#[wasm_bindgen]
pub struct WasmResult {
	pub status: WasmResultStatus,
	pub pointer: Option<*mut u8>,
	#[wasm_bindgen(getter_with_clone)]
	pub error_message: Option<String>,
}

impl WasmResult {
	pub fn ok() -> WasmResult {
		WasmResult { status: WasmResultStatus::Ok, pointer: None, error_message: None }
	}

	pub fn pointer<T>(value: T) -> WasmResult {
		WasmResult { status: WasmResultStatus::Pointer, pointer: Some(Box::into_raw(Box::new(value)) as *mut u8), error_message: None }
	}

	pub fn from_pointer_stream_output<T>(value: Option<T>) -> WasmResult {
		WasmResult { status: WasmResultStatus::Pointer, pointer: Some(Box::into_raw(Box::new(value)) as *mut u8), error_message: None }
	}

	pub fn pending() -> WasmResult {
		WasmResult { status: WasmResultStatus::Pending, pointer: None, error_message: None }
	}

	pub fn end_of_stream() -> WasmResult {
		WasmResult { status: WasmResultStatus::EndOfStream, pointer: None, error_message: None }
	}

	pub fn error<E: Debug>(status: WasmResultStatus, error_message: E) -> WasmResult {
		WasmResult { status, pointer: None, error_message: Some(format!("{:?}", error_message)) }
	}
}
