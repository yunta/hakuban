use std::sync::Arc;

use crate::object::ObjectState;

pub enum JsonDeserializeError {
	WrongFormat,
	SerdeError(serde_json::Error),
}

pub trait JsonDeserializeState {
	fn json_deserialize<O: serde::de::DeserializeOwned + 'static>(self) -> ObjectState<O>;
	fn try_json_deserialize<O: serde::de::DeserializeOwned + 'static>(self) -> Result<ObjectState<O>, JsonDeserializeError>;
}

impl JsonDeserializeState for ObjectState<Arc<Vec<u8>>> {
	fn json_deserialize<O: serde::de::DeserializeOwned + 'static>(self) -> ObjectState<O> {
		let ObjectState { data, version, mut format, synchronized } = self;
		if format.pop().unwrap().as_str() != "JSON" {
			panic!()
		};
		ObjectState { data: serde_json::from_slice(&data).unwrap(), version, format, synchronized }
	}

	fn try_json_deserialize<O: serde::de::DeserializeOwned + 'static>(self) -> Result<ObjectState<O>, JsonDeserializeError> {
		let ObjectState { data, version, mut format, synchronized } = self;
		if format.pop().ok_or(JsonDeserializeError::WrongFormat)?.as_str() != "JSON" {
			return Err(JsonDeserializeError::WrongFormat);
		};
		Ok(ObjectState { data: serde_json::from_slice(&data).map_err(JsonDeserializeError::SerdeError)?, version, format, synchronized })
	}
}

pub trait JsonSerializeState {
	fn json_serialize(self) -> ObjectState<Arc<Vec<u8>>>;
	fn try_json_serialize(self) -> Result<ObjectState<Arc<Vec<u8>>>, serde_json::Error>;
}

impl<T: serde::Serialize + 'static> JsonSerializeState for ObjectState<T> {
	fn json_serialize(self) -> ObjectState<Arc<Vec<u8>>> {
		let ObjectState { data, version, mut format, synchronized } = self;
		format.push("JSON".to_string());
		ObjectState { data: Arc::new(serde_json::to_vec(&data).unwrap()), version, format, synchronized }
	}

	fn try_json_serialize(self) -> Result<ObjectState<Arc<Vec<u8>>>, serde_json::Error> {
		let ObjectState { data, version, mut format, synchronized } = self;
		format.push("JSON".to_string());
		Ok(ObjectState { data: Arc::new(serde_json::to_vec(&data)?), version, format, synchronized })
	}
}
