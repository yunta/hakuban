#![allow(clippy::not_unsafe_ptr_arg_deref)]

use std::sync::Arc;

use futures::{stream::StreamExt, FutureExt};

use super::{ffi_future::FFIFuture, FFIObjectDescriptor, FFIObjectStateSink, FFIResult};
use crate::{Exchange, ObjectExposeContract};

pub struct FFIObjectExposeContract {
	contract: ObjectExposeContract,
	future: Arc<FFIFuture>,
}

#[no_mangle]
pub extern "C" fn hakuban_object_expose_contract_new(
	exchange: *mut Exchange,
	descriptor: *mut FFIObjectDescriptor,
	capacity: u32,
) -> *mut FFIObjectExposeContract {
	let exchange = unsafe { exchange.as_mut().unwrap() };
	let descriptor = unsafe { descriptor.as_mut().unwrap() };
	Box::into_raw(Box::new(FFIObjectExposeContract {
		contract: exchange.object_expose_contract(descriptor.descriptor.clone()).with_capacity(capacity).build(),
		future: FFIFuture::empty(),
	}))
}

#[no_mangle]
pub extern "C" fn hakuban_object_expose_contract_drop(object_expose_pointer: *mut FFIObjectExposeContract) {
	let object_expose_contract = unsafe { Box::from_raw(object_expose_pointer) };
	object_expose_contract.future.close();
	drop(object_expose_contract);
}

#[no_mangle]
pub extern "C" fn hakuban_object_expose_contract_next(object_expose_pointer: *mut FFIObjectExposeContract) -> *mut Arc<FFIFuture> {
	let object_expose_contract = unsafe { object_expose_pointer.as_mut().unwrap() };
	object_expose_contract.future = FFIFuture::new(
		object_expose_contract
			.contract
			.next()
			.map(|item| item.map(|sink| FFIResult::pointer(FFIObjectStateSink::new(sink))).unwrap_or(FFIResult::end_of_stream())),
	);
	Box::into_raw(Box::new(object_expose_contract.future.clone()))
}
