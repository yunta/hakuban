#![allow(clippy::not_unsafe_ptr_arg_deref)]

use std::sync::Arc;

use futures::{FutureExt, StreamExt};
use log::trace;

use super::{ffi_future::FFIFuture, FFIObjectStateSink, FFIResult, FFITagDescriptor};
use crate::{Exchange, TagExposeContract};

pub struct FFITagExposeContract {
	contract: TagExposeContract,
	future: Arc<FFIFuture>,
}

#[no_mangle]
pub extern "C" fn hakuban_tag_expose_contract_new(exchange: *mut Exchange, descriptor: *mut FFITagDescriptor, capacity: u32) -> *mut FFITagExposeContract {
	let exchange = unsafe { exchange.as_mut().unwrap() };
	let descriptor = unsafe { descriptor.as_mut().unwrap() };
	Box::into_raw(Box::new(FFITagExposeContract {
		contract: exchange.tag_expose_contract(descriptor.descriptor.clone()).with_capacity(capacity).build(),
		future: FFIFuture::empty(),
	}))
}

#[no_mangle]
pub extern "C" fn hakuban_tag_expose_contract_drop(tag_pointer: *mut FFITagExposeContract) {
	let tag_expose_contract = unsafe { Box::from_raw(tag_pointer) };
	tag_expose_contract.future.close();
	drop(tag_expose_contract);
}

#[no_mangle]
pub extern "C" fn hakuban_tag_expose_contract_next(tag_expose_pointer: *mut FFITagExposeContract) -> *mut Arc<FFIFuture> {
	let tag_expose_contract = unsafe { tag_expose_pointer.as_mut().unwrap() };
	tag_expose_contract.future = FFIFuture::new(
		tag_expose_contract
			.contract
			.next()
			.inspect(|item| {
				trace!("sscr {:?}", item.as_ref().map(|oss| oss.descriptor().clone()));
			})
			.map(|item| item.map(|sink| FFIResult::pointer(FFIObjectStateSink::new(sink))).unwrap_or(FFIResult::end_of_stream())),
	);
	Box::into_raw(Box::new(tag_expose_contract.future.clone()))
}
