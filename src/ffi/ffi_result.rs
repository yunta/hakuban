use std::{mem::MaybeUninit, sync::Arc};

#[derive(Debug, Eq, PartialEq)]
#[repr(u8)]
pub enum FFIResultStatus {
	Ok = 0,
	Pointer = 1,
	Pending = 2,
	EndOfStream = 3,
	InvalidString = 4,
	InvalidJSON = 5,
	InvalidURL = 6,
	InvalidLogLevel = 7,
	LoggerInitializationError = 8,
	ConnectionTerminated = 9,
}

#[derive(Debug)]
#[repr(C)]
pub struct FFIResult {
	pub status: FFIResultStatus,
	pub pointer: MaybeUninit<*mut u8>,
}

impl FFIResult {
	pub fn ok() -> FFIResult {
		FFIResult { status: FFIResultStatus::Ok, pointer: MaybeUninit::zeroed() }
	}

	pub fn pointer<T>(value: T) -> FFIResult {
		FFIResult { status: FFIResultStatus::Pointer, pointer: MaybeUninit::new(Box::into_raw(Box::new(value)) as *mut u8) }
	}

	pub fn arc_pointer<T>(value: Arc<T>) -> FFIResult {
		FFIResult { status: FFIResultStatus::Pointer, pointer: MaybeUninit::new(Arc::into_raw(value) as *mut u8) }
	}

	pub fn pending() -> FFIResult {
		FFIResult { status: FFIResultStatus::Pending, pointer: MaybeUninit::zeroed() }
	}

	pub fn end_of_stream() -> FFIResult {
		FFIResult { status: FFIResultStatus::EndOfStream, pointer: MaybeUninit::zeroed() }
	}

	pub fn error(status: FFIResultStatus) -> FFIResult {
		FFIResult { status, pointer: MaybeUninit::zeroed() }
	}
}
