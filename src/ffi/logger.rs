#![allow(clippy::not_unsafe_ptr_arg_deref)]

use std::ffi::CStr;

use super::FFIResult;

#[no_mangle]
pub extern "C" fn hakuban_logger_initialize(default_log_level: *const i8) -> FFIResult {
	let default_log_level = unsafe { CStr::from_ptr(default_log_level) };
	if let Ok(default_log_level) = default_log_level.to_str() {
		env_logger::Builder::from_env(env_logger::Env::default().default_filter_or(default_log_level)).init();
		FFIResult::ok()
	} else {
		FFIResult::error(super::FFIResultStatus::InvalidString)
	}
}
