#![allow(clippy::not_unsafe_ptr_arg_deref)]

use std::sync::Arc;

use futures::{FutureExt, StreamExt};

use super::{ffi_future::FFIFuture, FFIObjectStateStream, FFIResult, FFITagDescriptor};
use crate::{Exchange, TagObserveContract};

pub struct FFITagObserveContract {
	contract: TagObserveContract,
	future: Arc<FFIFuture>,
}

#[no_mangle]
pub extern "C" fn hakuban_tag_observe_contract_new(exchange: *mut Exchange, descriptor: *mut FFITagDescriptor) -> *mut FFITagObserveContract {
	let exchange = unsafe { exchange.as_mut().unwrap() };
	let descriptor = unsafe { descriptor.as_mut().unwrap() };
	Box::into_raw(Box::new(FFITagObserveContract {
		contract: exchange.tag_observe_contract(descriptor.descriptor.clone()).build(),
		future: FFIFuture::empty(),
	}))
}

#[no_mangle]
pub extern "C" fn hakuban_tag_observe_contract_drop(tag_pointer: *mut FFITagObserveContract) {
	let tag_observe_contract = unsafe { Box::from_raw(tag_pointer) };
	tag_observe_contract.future.close();
	drop(tag_observe_contract);
}

#[no_mangle]
pub extern "C" fn hakuban_tag_observe_contract_next(tag_observe_pointer: *mut FFITagObserveContract) -> *mut Arc<FFIFuture> {
	let tag_observe_contract = unsafe { tag_observe_pointer.as_mut().unwrap() };
	tag_observe_contract.future = FFIFuture::new(
		tag_observe_contract
			.contract
			.next()
			.map(|item| item.map(|stream| FFIResult::pointer(FFIObjectStateStream::new(stream))).unwrap_or(FFIResult::end_of_stream())),
	);
	Box::into_raw(Box::new(tag_observe_contract.future.clone()))
}
