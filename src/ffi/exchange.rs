//if we don't mark pub function as unsafe when it has unsafe sections, clippy will complain
//if we put an unsafe block inside an unsafe function, clippy will complain too
//we'd rather have every unsafe block explicit, so disabling warning that function is not unsafe
#![allow(clippy::not_unsafe_ptr_arg_deref)]

use log::trace;

use super::ffi_result::FFIResult;
use crate::Exchange;

#[no_mangle]
pub extern "C" fn hakuban_exchange_new() -> FFIResult {
	trace!("Constructing exchange");
	let exchange = Exchange::new();
	FFIResult::pointer(exchange)
}

#[no_mangle]
pub extern "C" fn hakuban_exchange_drop(exchange: *mut Exchange) {
	let exchange = unsafe { Box::from_raw(exchange) };
	trace!("Dropping exchange");
	drop(exchange);
}

#[no_mangle]
pub extern "C" fn hakuban_exchange_clone(exchange_pointer: *mut Exchange) -> *mut Exchange {
	let exchange: &mut Exchange = unsafe { exchange_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(exchange.clone()))
}
