#![allow(clippy::not_unsafe_ptr_arg_deref)]

use std::sync::Arc;

use futures::{stream::StreamExt, FutureExt};

use super::{ffi_future::FFIFuture, FFIObjectDescriptor, FFIObjectState, FFIResult};
use crate::object::ObjectStateStream;

pub struct FFIObjectStateStream {
	stream: ObjectStateStream,
	future: Arc<FFIFuture>,
}

impl FFIObjectStateStream {
	pub(super) fn new(stream: ObjectStateStream) -> FFIObjectStateStream {
		FFIObjectStateStream { stream, future: FFIFuture::empty() }
	}
}

#[no_mangle]
pub extern "C" fn hakuban_object_state_stream_drop(object_state_stream_pointer: *mut FFIObjectStateStream) {
	let object_state_stream = unsafe { Box::from_raw(object_state_stream_pointer) };
	object_state_stream.future.close();
	drop(object_state_stream);
}

#[no_mangle]
pub extern "C" fn hakuban_object_state_stream_next(object_state_stream_pointer: *mut FFIObjectStateStream) -> *mut Arc<FFIFuture> {
	let object_state_stream = unsafe { object_state_stream_pointer.as_mut().unwrap() };
	object_state_stream.future = FFIFuture::new(
		object_state_stream.stream.next().map(|item| item.map(|state| FFIResult::pointer(FFIObjectState::new(state))).unwrap_or(FFIResult::end_of_stream())),
	);
	Box::into_raw(Box::new(object_state_stream.future.clone()))
}

#[no_mangle]
pub extern "C" fn hakuban_object_state_stream_descriptor(object_state_stream_pointer: *mut FFIObjectStateStream) -> *mut FFIObjectDescriptor {
	let object_state_stream = unsafe { object_state_stream_pointer.as_mut().unwrap() };
	Box::into_raw(Box::new(object_state_stream.stream.descriptor().into()))
}
