#![allow(clippy::not_unsafe_ptr_arg_deref)]

use std::sync::Arc;

use futures::{stream::StreamExt, FutureExt};

use super::{ffi_future::FFIFuture, FFIObjectDescriptor, FFIObjectStateStream, FFIResult};
use crate::{Exchange, ObjectObserveContract};

pub struct FFIObjectObserveContract {
	contract: ObjectObserveContract,
	future: Arc<FFIFuture>,
}

#[no_mangle]
pub extern "C" fn hakuban_object_observe_contract_new(exchange: *mut Exchange, descriptor: *mut FFIObjectDescriptor) -> *mut FFIObjectObserveContract {
	let exchange = unsafe { exchange.as_mut().unwrap() };
	let descriptor = unsafe { descriptor.as_mut().unwrap() };
	Box::into_raw(Box::new(FFIObjectObserveContract {
		contract: exchange.object_observe_contract(descriptor.descriptor.clone()).build(),
		future: FFIFuture::empty(),
	}))
}

#[no_mangle]
pub extern "C" fn hakuban_object_observe_contract_drop(object_ptr: *mut FFIObjectObserveContract) {
	let object_observe_contract = unsafe { Box::from_raw(object_ptr) };
	object_observe_contract.future.close();
	drop(object_observe_contract);
}

#[no_mangle]
pub extern "C" fn hakuban_object_observe_contract_next(object_observe_pointer: *mut FFIObjectObserveContract) -> *mut Arc<FFIFuture> {
	let object_observe_contract = unsafe { object_observe_pointer.as_mut().unwrap() };
	object_observe_contract.future = FFIFuture::new(
		object_observe_contract
			.contract
			.next()
			.map(|item| item.map(|stream| FFIResult::pointer(FFIObjectStateStream::new(stream))).unwrap_or(FFIResult::end_of_stream())),
	);
	Box::into_raw(Box::new(object_observe_contract.future.clone()))
}
