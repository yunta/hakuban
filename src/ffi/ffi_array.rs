use std::ffi::c_void;

#[repr(C)]
pub struct FFIBorrowedArray {
	pub length: usize,
	pub pointer: *mut c_void,
}

#[repr(C)]
pub struct FFIOwnedArray {
	pub length: usize,
	pub pointer: *mut c_void,
}

#[no_mangle]
pub extern "C" fn hakuban_array_drop(array: FFIOwnedArray) {
	drop(unsafe { Vec::from_raw_parts(array.pointer, array.length, array.length) });
}
